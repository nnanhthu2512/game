/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataCacche.cs 								                    */
/* Description : Classes and functions to cache data for both web and winform   */
/* application                                                                  */
/*                                                                              */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 05.Oct.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using CommonLibs;
using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using Game;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;

namespace DataCache
{
    internal static class Logs
    {
        const string DEBUG_LOG = "cache.txt";
        internal static void WriteDebugLog(string msg)
        {
            if (Settings.sysDebugMode) ErrorLibs.WriteLog(msg, DEBUG_LOG);
        }
    }

    /// <summary>
    /// Template to find and cache data at 2 levels
    /// - local cache (at local application if any)
    /// - global cache (central cache)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal abstract class FindAndCache<T>
    {
        internal FindAndCache(string name, CommonTypes.CacheType type, int _cacheTimeInSecs)
        {
            this.Name = name;
            this.CacheType = type;
            this.cacheTimeInSecs = _cacheTimeInSecs;
        }
        internal bool UseLocalCache = true;
        string Name = null;
        CommonTypes.CacheType CacheType;
        int cacheTimeInSecs;

        //To enable local cache : override the functions bellow.
        protected virtual T GetFromLocalCache(params string[] key) { return default(T); }
        protected virtual bool PutToLocalCache(T info, params string[] key) { return true; }
        protected virtual bool RemoveFromLocalCache(params string[] key) { return true; }
        protected virtual bool ClearLocalCache() { return true; }
        protected virtual bool ExistedInLocalCache(params string[] key) { return false; }

        /// <summary>
        /// Local cached data is fresh (not stale/changed since putting in cache)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual bool LocalCacheIsFresh(params string[] key)
        {
            return true;
        }

        private T GetFromGlobalCache(params string[] key)
        {
            string cacheKey = SysLibs.MakeCacheKey(this.Name, key);
            if (string.IsNullOrWhiteSpace(cacheKey)) return default(T);
            T info;
            if (Libs.Get(cacheKey, out info, this.CacheType)) return info;
            return default(T);
        }
        internal bool PutToGlobalCache(T info, params string[] key)
        {
            string cacheKey = SysLibs.MakeCacheKey(this.Name, key);
            if (string.IsNullOrWhiteSpace(cacheKey)) return false;
            if (Libs.Put(info, cacheKey, this.CacheType, this.cacheTimeInSecs) == false)
            {
                ErrorLibs.WriteErrorLog(string.Format("PutToGlobalCache error on key {0}", cacheKey));
                return false;
            }
            return true;
        }
        bool ExistedInGlobalCache(params string[] key)
        {
            string cacheKey = SysLibs.MakeCacheKey(this.Name, key);
            if (string.IsNullOrWhiteSpace(cacheKey)) return false;
            return Libs.Exists(cacheKey, this.CacheType);
        }

        //Load from db
        protected abstract T RetrieveFromStorage(params string[] key);

        internal bool Existed(params string[] key)
        {
            return this.Get(key) != null;
        }

        internal bool Put(T info, params string[] key)
        {
            if (this.PutToLocalCache(info, key) == false)
            {
                ErrorLibs.WriteErrorLog(string.Format("Update to local cache error on key {0}", SysLibs.MakeCacheKey(this.Name, key)));
                return false;
            }
            if (this.PutToGlobalCache(info, key) == false) return false;
            return true;
        }

        internal T Get(params string[] key)
        {
            T info = default(T);
            if (this.UseLocalCache)
            {
                info = this.GetFromLocalCache(key);
                if (info != null)
                {
                    if (this.LocalCacheIsFresh(key) == false)
                    {
                        if (RemoveFromLocalCache(key) == false)
                        {
                            Logs.WriteDebugLog(string.Format("Remove From Local Cache {0}", SysLibs.MakeCacheKey(this.Name, key)));
                            return info;
                        }
                        return this.RetrieveAndAddToCache(key);
                    }
                    Logs.WriteDebugLog(string.Format("Get From Local Cache {0}", SysLibs.MakeCacheKey(this.Name, key)));
                    return info;
                }
            }
            info = this.GetFromGlobalCache(key);
            if (info != null)
            {
                Logs.WriteDebugLog(string.Format("GetFromGlobalCache key {0}", SysLibs.MakeCacheKey(this.Name, key)));
                this.PutToLocalCache(info, key);
                return info;
            }
            return this.RetrieveAndAddToCache(key);
        }

        private T RetrieveAndAddToCache(params string[] key)
        {
            Logs.WriteDebugLog(string.Format("RetrieveFromStorage to key {0}", SysLibs.MakeCacheKey(this.Name, key)));
            T info = this.RetrieveFromStorage(key);
            if (info == null) return default(T);
            if (this.UseLocalCache)
            {
                if (this.PutToLocalCache(info, key) == false)
                {
                    ErrorLibs.WriteErrorLog(string.Format("Update to local cache error on key {0}", SysLibs.MakeCacheKey(this.Name, key)));
                    return default(T);
                }
            }
            this.PutToGlobalCache(info, key);
            return info;
        }

        internal bool Remove(params string[] key)
        {
            if (this.RemoveFromLocalCache(key) == false)
            {
                ErrorLibs.WriteErrorLog(string.Format("Remove From Local Cache error {0}", SysLibs.MakeCacheKey(this.Name, key)));
                return false;
            }
            if (Libs.Remove(SysLibs.MakeCacheKey(this.Name, key)) == false)
            {
                ErrorLibs.WriteErrorLog(string.Format("Remove from global cache error {0} ", SysLibs.MakeCacheKey(this.Name, key)));
                return false;
            }
            return true;
        }

        internal bool ClearAll()
        {
            if (this.ClearLocalCache() == false)
            {
                ErrorLibs.WriteDebugLog(string.Format("Clear local cache error"));
                return false;
            }
            //Donot clear global cache
            //Libs.Reset(this.CacheType);
            return true;
        }
    }

    internal abstract class BaseCache
    {
        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs
        /// </summary>
        /// <param name="o">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public abstract bool Add<T>(T o, string key, int? expiryInSecs = null);

        /// <summary>
        /// Add or update value into the cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o"></param>
        /// <param name="key"></param>
        public abstract bool Update<T>(T o, string key, int? expiryInSecs = null);

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public abstract bool Remove(string key);

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public abstract bool Exists(string key);

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
        /// <returns>Cached item as type</returns>
        public abstract bool Get<T>(string key, out T value);

        public virtual SortedList<string, T> GetMulti<T>(List<string> keys)
        {
            return null;
        }

        public abstract void ClearAll();
        public abstract void ClearCache(string region, CommonTypes.CacheType type);
    }

    //internal class ReDisCache : BaseCache
    //{
    //    public ReDisCache()
    //    {
    //    }

    //    ConnectionMultiplexer _myConnection = null;
    //    ConnectionMultiplexer myConnection
    //    {
    //        get
    //        {
    //            if (_myConnection == null || _myConnection.IsConnected == false)
    //            {
    //                _myConnection = ConnectionMultiplexer.Connect(CommonTypes.SysLibs.sysRedisConnectionStr);
    //                _myCache = null;
    //            }
    //            return _myConnection;
    //        }
    //    }

    //    IDatabase _myCache = null;
    //    IDatabase myCache
    //    {
    //        get
    //        {
    //            if (_myCache == null)
    //            {
    //                _myCache = this.myConnection.GetDatabase();
    //            }
    //            return _myCache;
    //        }
    //    }

    //    /// <summary>
    //    /// Insert value into the cache using appropriate name/value pairs
    //    /// </summary>
    //    /// <param name="o">Item to be cached</param>
    //    /// <param name="key">Name of item</param>
    //    public override bool Add<T>(T o, string key, int? expiryInSecs = null)
    //    {
    //        if (expiryInSecs == null) expiryInSecs = CommonTypes.Settings.RedisCacheTimeInSecs;
    //        if (typeof(T) == typeof(string))
    //            this.myCache.StringSet(key, o.ToString(), TimeSpan.FromSeconds(CommonTypes.Settings.RedisCacheTimeInSecs));
    //        else
    //            this.myCache.StringSet(key, CommonTypes.Transform.Serialize(o), TimeSpan.FromSeconds(CommonTypes.Settings.RedisCacheTimeInSecs));
    //        return true;
    //    }

    //    /// <summary>
    //    /// Add or update value into the cache
    //    /// </summary>
    //    /// <typeparam name="T"></typeparam>
    //    /// <param name="o"></param>
    //    /// <param name="key"></param>
    //    public override bool Update<T>(T o, string key, int? expiryInSecs = null)
    //    {
    //        return  Add(o, key, expiryInSecs);
    //    }

    //    /// <summary>
    //    /// Remove item from cache
    //    /// </summary>
    //    /// <param name="key">Name of cached item</param>
    //    public override bool Remove(string key)
    //    {
    //        if (this.Exists(key)) return this.myCache.KeyDelete(key);
    //        return true;
    //    }

    //    /// <summary>
    //    /// Check for item in cache
    //    /// </summary>
    //    /// <param name="key">Name of cached item</param>
    //    /// <returns></returns>
    //    public override bool Exists(string key)
    //    {
    //        return this.myCache.KeyExists(key);
    //    }

    //    /// <summary>
    //    /// Retrieve cached item
    //    /// </summary>
    //    /// <typeparam name="T">Type of cached item</typeparam>
    //    /// <param name="key">Name of cached item</param>
    //    /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
    //    /// <returns>Cached item as type</returns>
    //    public override bool Get<T>(string key, out T value)
    //    {
    //        if (Exists(key) == false)
    //        {
    //            value = default(T);
    //            return false;
    //        }
    //        if (typeof(T) == typeof(string))
    //            value = (T)Convert.ChangeType(this.myCache.StringGet(key), typeof(T));
    //        else value = CommonTypes.Transform.Deserialize<T>((byte[])this.myCache.StringGet(key));
    //        return true;
    //    }

    //    public override void ClearAll()
    //    {
    //        System.Net.EndPoint[] endpoints = this.myConnection.GetEndPoints(true);
    //        foreach (var endpoint in endpoints)
    //        {
    //            var server = this.myConnection.GetServer(endpoint);
    //            server.FlushAllDatabases();
    //        }
    //    }

    //    public override void ClearCache(string region, CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
    //    {
    //        if (region == null) return;
    //        string keyPattern = Libs.GetCacheKey(region, type) + "*";

    //        System.Net.EndPoint[] endpoints = this.myConnection.GetEndPoints(true);
    //        foreach (var endpoint in endpoints)
    //        {
    //            var server = this.myConnection.GetServer(endpoint);
    //            var keys = server.Keys(pattern: keyPattern);
    //            foreach (RedisKey item in keys)
    //            {
    //                this.myCache.KeyDelete(item);
    //            }
    //        }
    //    }
    //}

    //internal class WebCache : BaseCache
    //{
    //    /// <summary>
    //    /// Insert value into the cache using appropriate name/value pairs
    //    /// </summary>
    //    /// <param name="o">Item to be cached</param>
    //    /// <param name="key">Name of item</param>
    //    public override bool Add<T>(T o, string key, int? expiryInSecs = null)
    //    {
    //        if (expiryInSecs == null) expiryInSecs = CommonTypes.Settings.sysCacheSlidingExpirationInSecs;
    //        // NOTE: Apply expiration parameters as you see fit.
    //        // I typically pull from configuration file.

    //        // In this example, I want an absolute
    //        // timeout so changes will always be reflected
    //        // at that time. Hence, the NoSlidingExpiration.
    //        HttpContext.Current.Cache.Insert(
    //            key,
    //            o,
    //            null,
    //            System.Web.Caching.Cache.NoAbsoluteExpiration,
    //            TimeSpan.FromSeconds(expiryInSecs.Value));
    //        return true;
    //    }

    //    /// <summary>
    //    /// Add or update value into the cache
    //    /// </summary>
    //    /// <typeparam name="T"></typeparam>
    //    /// <param name="o"></param>
    //    /// <param name="key"></param>
    //    public override bool Update<T>(T o, string key, int? expiryInSecs = null)
    //    {
    //        if (this.Exists(key))
    //        {
    //            HttpContext.Current.Cache[key] = o;
    //            return true;
    //        }
    //        return Add(o, key, expiryInSecs);
    //    }

    //    /// <summary>
    //    /// Remove item from cache
    //    /// </summary>
    //    /// <param name="key">Name of cached item</param>
    //    public override bool Remove(string key)
    //    {
    //        if (this.Exists(key)) HttpContext.Current.Cache.Remove(key);
    //        return true;
    //    }

    //    /// <summary>
    //    /// Check for item in cache
    //    /// </summary>
    //    /// <param name="key">Name of cached item</param>
    //    /// <returns></returns>
    //    public override bool Exists(string key)
    //    {
    //        return HttpContext.Current.Cache[key] != null;
    //    }

    //    /// <summary>
    //    /// Retrieve cached item
    //    /// </summary>
    //    /// <typeparam name="T">Type of cached item</typeparam>
    //    /// <param name="key">Name of cached item</param>
    //    /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
    //    /// <returns>Cached item as type</returns>
    //    public override bool Get<T>(string key, out T value)
    //    {
    //        if (Exists(key) == false)
    //        {
    //            value = default(T);
    //            return false;
    //        }
    //        value = (T)HttpContext.Current.Cache[key];
    //        return true;
    //    }

    //    public override void ClearAll()
    //    {
    //        foreach (DictionaryEntry entry in HttpContext.Current.Cache)
    //        {
    //            HttpContext.Current.Cache.Remove(entry.Key.ToString());
    //        }
    //    }

    //    public override void ClearCache(string region, CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
    //    {
    //        if (region == null) return;
    //        foreach (DictionaryEntry entry in HttpContext.Current.Cache)
    //        {
    //            if (entry.Key.ToString().StartsWith(region) == false) continue;
    //            HttpContext.Current.Cache.Remove(entry.Key.ToString());
    //        }
    //    }
    //}

    //internal class SysCache : BaseCache
    //{
    //    /// <summary>
    //    /// Insert value into the cache using
    //    /// appropriate name/value pairs
    //    /// </summary>
    //    /// <typeparam name="T">Type of cached item</typeparam>
    //    /// <param name="o">Item to be cached</param>
    //    /// <param name="key">Name of item</param>
    //    public override bool Add<T>(T o, string key, int? expiryInSecs = null)
    //    {
    //        if (expiryInSecs == null) expiryInSecs = CommonTypes.Settings.sysCacheAbsoluteExpirationInSecs;
    //        // NOTE: Apply expiration parameters as you see fit.
    //        // I typically pull from configuration file.
    //        return MemoryCache.Default.Add(key, o, CommonTypes.SysLibs.GetCurrentTime().AddSeconds(expiryInSecs.Value));
    //    }

    //    public override bool Update<T>(T o, string key, int? expiryInSecs = null)
    //    {
    //        if (this.Exists(key)) this.Remove(key);
    //        return  Add(o, key, expiryInSecs);
    //    }

    //    /// <summary>
    //    /// Remove item from cache
    //    /// </summary>
    //    /// <param name="key">Name of cached item</param>
    //    public override bool Remove(string key)
    //    {
    //        if (this.Exists(key)) MemoryCache.Default.Remove(key);
    //        return true;
    //    }

    //    /// <summary>
    //    /// Check for item in cache
    //    /// </summary>
    //    /// <param name="key">Name of cached item</param>
    //    /// <returns></returns>
    //    public override bool Exists(string key)
    //    {
    //        return MemoryCache.Default[key] != null;
    //    }

    //    /// <summary>
    //    /// Retrieve cached item
    //    /// </summary>
    //    /// <typeparam name="T">Type of cached item</typeparam>
    //    /// <param name="key">Name of cached item</param>
    //    /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
    //    /// <returns>Cached item as type</returns>
    //    public override bool Get<T>(string key, out T value)
    //    {
    //        if (!Exists(key))
    //        {
    //            value = default(T);
    //            return false;
    //        }
    //        value = (T)MemoryCache.Default[key];
    //        return true;
    //    }

    //    public override void ClearAll()
    //    {
    //        //MemoryCache.Default.Dispose();
    //        //MemoryCache.Default = new MemoryCache(null);
    //        //List<string> cacheKeys = MemoryCache.Default..Select(kvp => kvp.Key).ToList();
    //        //foreach (string cacheKey in cacheKeys)
    //        //{
    //        //    MemoryCache.Default.Remove(cacheKey);
    //        //}
    //        //Parallel.ForEach(allKeys, key => _cache.Remove(key));
    //    }

    //    public override void ClearCache(string region, CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
    //    {
    //    }
    //}

    internal class MemCache : BaseCache
    {
        const short MAX_TRY_ON_ERROR = 10;
        public MemCache()
        {
        }

        MemcachedClient _myCacheClient = null;
        MemcachedClient myCacheClient
        {
            get
            {
                if (_myCacheClient == null) InitClient();
                return _myCacheClient;
            }
        }
        /// <summary>
        /// See https://github.com/enyim/EnyimMemcached/wiki/MemcachedClient-Configuration
        /// </summary>
        void InitClient()
        {
            MemcachedClientConfiguration config = new MemcachedClientConfiguration();
            foreach (string ip in Settings.sysMemcacheServerAddr)
            {
                ErrorLibs.WriteLog(String.Format("Using memcache server: {0}", ip), "memcache.log.txt");
                config.Servers.Add(new IPEndPoint(IPAddress.Parse(ip), Settings.sysMemcacheServerPort));
            }
            config.Protocol = MemcachedProtocol.Binary;
            _myCacheClient = new MemcachedClient(config);
        }

        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs
        /// </summary>
        /// <param name="o">Item to be cached</param>
        /// <param name="key">Name of item</param>

        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs
        /// </summary>
        /// <param name="o">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public override bool Add<T>(T o, string key, int? expiryInSecs = null)
        {
            if (expiryInSecs == null) expiryInSecs = Settings.sysMemcacheTimeInSecs;
            lock (myCacheClient)
            {
                int count = MAX_TRY_ON_ERROR;
                while (count-- > 0)
                {
                    bool status = myCacheClient.Store(StoreMode.Set, key, o, TimeSpan.FromSeconds(expiryInSecs.Value));
                    if (status) return true;
                    Remove(key);
                }
                return false;
            }
        }

        /// <summary>
        /// Add or update value into the cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o"></param>
        /// <param name="key"></param>
        public override bool Update<T>(T o, string key, int? expiryInSecs = null)
        {
            return Add(o, key, expiryInSecs);
        }

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public override bool Remove(string key)
        {
            lock (myCacheClient)
            {
                myCacheClient.Remove(key);
                return true;
            }
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public override bool Exists(string key)
        {
            lock (myCacheClient)
            {
                // Memcached add command is used to set a value to a new key. 
                // If the key already exists, then it gives the output NOT_STORED.
                if (myCacheClient.Store(StoreMode.Add, key, null))
                {
                    myCacheClient.Remove(key);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
        /// <returns>Cached item as type</returns>
        public override bool Get<T>(string key, out T value)
        {
            value = (T)myCacheClient.Get(key);
            return value != null;
        }

        public override SortedList<string, T> GetMulti<T>(List<string> keys)
        {
            SortedList<string, T> list = new SortedList<string, T>();
            foreach (string key in keys)
            {
                T info = myCacheClient.Get<T>(key);
                if (info != null) list.Add(key, info);
            }
            return list;
        }

        public override void ClearAll()
        {
            myCacheClient.FlushAll();
        }

        public override void ClearCache(string region, CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
        {
        }
    }

    public static class Libs
    {
        private static MemCache _myCacheStorageMem = null;

        internal static MemCache myCacheStorageMem
        {
            get
            {
                if (_myCacheStorageMem == null) _myCacheStorageMem = new MemCache();
                return _myCacheStorageMem;
            }
        }

        static CommonTypes.CacheType GetCacheAutoType()
        {
            if (Settings.sysMemcacheEnabled) return CommonTypes.CacheType.MemCache;
            if (HttpContext.Current != null) return CommonTypes.CacheType.WebCache;
            return CommonTypes.CacheType.System;
        }
        internal static BaseCache GetCacheStorage(CommonTypes.CacheType type)
        {
            if (type == CommonTypes.CacheType.Auto) type = GetCacheAutoType();
            switch (type)
            {
                case CommonTypes.CacheType.MemCache: return myCacheStorageMem;
                default:
                    ErrorLibs.WriteErrorLog("CommonTypes.Settings.sysUseCacheType is not defined"); break;
            }
            return null;
        }

        static string GetCacheRegionName(CommonTypes.CacheType type, bool cacheWithSession = false)
        {
            if (type == CommonTypes.CacheType.Auto) type = GetCacheAutoType();

            if (cacheWithSession && type == CommonTypes.CacheType.MemCache &&
                HttpContext.Current != null && HttpContext.Current.Request.Cookies[SysLibs.GetAuthCookieName()] != null)
                return CommonLibs.Hash.MakeHash(HttpContext.Current.Request.Cookies[SysLibs.GetAuthCookieName()].Value);

            if (type == CommonTypes.CacheType.WebCache &&
                HttpContext.Current != null && HttpContext.Current.Request.Cookies[SysLibs.GetAuthCookieName()] != null)
                return HttpContext.Current.Request.Cookies[SysLibs.GetAuthCookieName()].Value;

            return type.ToString();
        }

        internal static string GetCacheKey(string key, CommonTypes.CacheType type, bool cacheWithSession = false)
        {
            return GetCacheKey(new List<string> { key }, type, cacheWithSession)[0];
        }
        internal static List<string> GetCacheKey(List<string> keys, CommonTypes.CacheType type, bool cacheWithSession = false)
        {
            List<string> cacheKeys = new List<string>();
            foreach (string key in keys)
            {
                cacheKeys.Add(Game.Consts.ProductCode + "." + GetCacheRegionName(type, cacheWithSession) + "." + key);
            }
            return cacheKeys;
        }

        internal static bool Exists(string key, CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
        {
            key = GetCacheKey(key, type);
            return GetCacheStorage(type).Exists(key);
        }
        public static bool Put<T>(T o, string key, CommonTypes.CacheType type = CommonTypes.CacheType.Auto, int? expiryInSecs = null, bool cacheWithSession = false)
        {
            try
            {
                key = GetCacheKey(key, type, cacheWithSession);
                return GetCacheStorage(type).Update(o, key, expiryInSecs);
            }
            catch (Exception er)
            {
                ErrorLibs.WriteErrorLog(er);
                return false;
            }
        }
        public static bool Get<T>(string key, out T value, CommonTypes.CacheType type = CommonTypes.CacheType.Auto, bool cacheWithSession = false)
        {
            key = GetCacheKey(key, type, cacheWithSession);
            return GetCacheStorage(type).Get(key, out value);
        }
        public static bool Remove(string key, CommonTypes.CacheType type = CommonTypes.CacheType.Auto, bool cacheWithSession = false)
        {
            key = GetCacheKey(key, type, cacheWithSession);
            return GetCacheStorage(type).Remove(key);
        }

        public static SortedList<string, T> GetMulti<T>(List<string> keys, CommonTypes.CacheType type = CommonTypes.CacheType.Auto, bool cacheWithSession = false)
        {
            List<string> cacheKeys = GetCacheKey(keys, type, cacheWithSession);
            return GetCacheStorage(type).GetMulti<T>(cacheKeys);
        }

        //Do nothing
        internal static void ClearCache()
        {
        }

        public static void ClearCache(CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
        {
            GetCacheStorage(type).ClearCache(GetCacheRegionName(type), type);
        }

        internal static void ClearCache(string key, CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
        {
            GetCacheStorage(type).ClearCache(key, type);
        }

        public static void Reset(CommonTypes.CacheType type = CommonTypes.CacheType.Auto)
        {
            CommonLibs.ErrorLibs.WriteDebugLog("Clear All cache " + type);
            GetCacheStorage(type).ClearAll();
        }

        /// <summary>
        /// Function to keep track of cached items that was updated by the system
        /// </summary>
        internal static class CacheTrack
        {
            static SortedList<string, long> CacheStampList = new SortedList<string, long>();

            private static string GetCacheKey(string key, string id)
            {
                return string.Format("{0}-{1}", key, id);
            }

            internal static bool Add(string key, string id, bool toLocal, bool toCloud)
            {
                if (Settings.sysCacheKeepTrackDataInSecs <= 0) return true;

                key = GetCacheKey(key, id);
                long value = DateTime.UtcNow.Ticks;
                if (toCloud)
                {
                    Logs.WriteDebugLog(string.Format("CacheTrack {0} {1}={2}", key, key, value));
                    Libs.Put(value.ToString(), key, CommonTypes.CacheType.Auto, Settings.sysCacheKeepTrackDataInSecs);
                }
                if (toLocal)
                {
                    if (CacheStampList.ContainsKey(key)) CacheStampList[key] = value;
                    else CacheStampList.Add(key, value);
                }
                return true;
            }

            /// <summary>
            /// Return True if data in local cache are different from the system database
            /// </summary>
            /// <param name="key"></param>
            /// <param name="id"></param>
            /// <returns></returns>
            internal static bool DataChanged(string key, string id)
            {
                if (Settings.sysCacheKeepTrackDataInSecs <= 0) return false;
                key = GetCacheKey(key, id);
                string updateTicks = "";
                if (DataCache.Libs.Get(key, out updateTicks, CommonTypes.CacheType.MemCache) == false) return false;
                if (CacheStampList.ContainsKey(key) == false) return true;
                long updateTicksValue = 0;
                if (long.TryParse(updateTicks, out updateTicksValue) == false) return true;

                return updateTicksValue > CacheStampList[key];
            }
        }
    }
}

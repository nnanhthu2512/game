﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Game
{
    public static class ExtendCtrl
    {
        public static bool CreateGdrUser(BO.UserSubmit info)
        {
            //Call function create user from GDR
            const string NOTI_CONTROLLER = "Provider/GetGameResult";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Game.Settings.GdrApiURL);
                
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                CommonLibs.ErrorLibs.WriteErrorLog("1 GdrApiURL: " + Game.Settings.GdrApiURL);
                HttpResponseMessage response = client.PostAsJsonAsync(NOTI_CONTROLLER, info).Result;
                CommonLibs.ErrorLibs.WriteErrorLog("2 GdrApiURL: " + response.RequestMessage.RequestUri.OriginalString);
                if (response.IsSuccessStatusCode == false)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(string.Format("MakeConnection failed with code {0}", response));
                    return false;
                }
                return response.IsSuccessStatusCode;
            }
        }
    }
}

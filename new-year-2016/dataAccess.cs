﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataEngine.cs 								                    */
/* Description : Common classes and functions supporting to retrieve/search     */
/* module.                                                                      */
/*													                            */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Game;
using System.Text.RegularExpressions;
using static Game.BL;

namespace DataAccess
{
    internal class PhoneNo
    {
        public string ShortPhoneNo;
        public string PostCode;
        public string CountryCode;

        public bool Parse(string str, string countryCode = null)
        {
            bool fProcess = false;
            CountrySettings countrySetting = new CountrySettings();
            str = str.Trim();
            if (str.StartsWith("+"))
            {
                str = RemoveInvalidChars(str.Substring(1));
                if (Game.BL.ValidateLibs.ValidatePhoneNo(str) == false) return false;
                string postCode = GetPostCodeFromPhoneNo(str);
                if (string.IsNullOrWhiteSpace(postCode) == false && countrySetting.LoadByPostCode(postCode))
                {
                    this.PostCode = postCode;
                    this.ShortPhoneNo = str.Substring(this.PostCode.Length);
                    this.CountryCode = countrySetting.Country;
                    fProcess = true;
                }
            }

            if (fProcess == false && str.StartsWith("("))
            {
                string[] items = str.Split(new string[] { ")" }, StringSplitOptions.RemoveEmptyEntries);
                if (items.Length != 2) return false;

                items[0] = RemoveInvalidChars(items[0].Substring(1));
                items[1] = RemoveInvalidChars(items[1]);
                if (Game.BL.ValidateLibs.ValidatePhoneNo(items[1]) == false) return false;
                this.ShortPhoneNo = items[1];

                this.PostCode = items[0];
                if (this.PostCode.StartsWith("+")) this.PostCode = this.PostCode.Substring(1);

                if (countrySetting.LoadByPostCode(this.PostCode) == false) return false;
                this.CountryCode = countrySetting.Country;
                fProcess = true;
            }

            if (fProcess == false && string.IsNullOrWhiteSpace(countryCode) == false)
            {
                if (Game.BL.ValidateLibs.ValidatePhoneNo(str) == false) return false;
                if (countrySetting.LoadByCountry(countryCode) == false) return false;
                this.CountryCode = countryCode;
                this.ShortPhoneNo = RemoveInvalidChars(str);
                this.PostCode = countrySetting.PostCode;
            }

            if (string.IsNullOrWhiteSpace(this.CountryCode) || string.IsNullOrWhiteSpace(this.ShortPhoneNo) ||
                string.IsNullOrWhiteSpace(this.PostCode)) return false;

            //Remove '0' at the start of the phone no
            //if (this.ShortPhoneNo.StartsWith("0")) this.ShortPhoneNo = this.ShortPhoneNo.Substring(1);

            return true;
        }

        internal static string RemoveInvalidChars(string str)
        {
            return str.Replace(" ", "");
        }

        internal static string GetPostCodeFromPhoneNo(string phoneNo)
        {
            if (string.IsNullOrWhiteSpace(phoneNo)) return null;
            foreach (game.myGame.sysCountrySettingsRow row in DataAccess.DbCache.CountrySettings)
            {
                if (phoneNo.StartsWith(row.postCode)) return row.postCode;
            }
            return null;
        }
    }


    public class CountrySettings : CommonTypes.BaseObject
    {
        private string CultureCode = null;
        public string Country = Game.Consts.DefaultCountry;
        public string PostCode = "";

        //public CultureInfo CultureInfo
        //{
        //    get
        //    {
        //        if (String.IsNullOrWhiteSpace(CultureCode)) CultureCode = CommonTypes.SysLibs.GetCultureCode(CommonTypes.Consts.DefaultLanguage);
        //        return CommonTypes.SysLibs.GetLanguageInfo(CommonTypes.SysLibs.GetLanguage(this.CultureCode)).Culture;
        //    }
        //}

        //public string webAmtMask = "999.999.999.999.999";
        //public string webPhoneMask = "9999-9999";

        //public string netAmtMask = CommonTypes.Consts.DefaultAmtFormat;
        public string netPhoneMask = Game.Consts.DefaultPhoneFormat;
        //public string zipCodeMask = "#####-###";

        //public string DateInputMask = "m/d/y";
        //public string TimeInputMask = "h:s";

        //public string DateOutputMask = "MM/DD/YYYY";
        //public string TimeOutputMask = "HH:SS";

        //public string DateTimeInputMask
        //{
        //    get
        //    {
        //        if (String.IsNullOrWhiteSpace(this.DateInputMask) || String.IsNullOrWhiteSpace(this.TimeInputMask)) return null;
        //        return this.DateInputMask + " " + this.TimeInputMask;
        //    }
        //}
        //public string DateTimeOutputMask
        //{
        //    get
        //    {
        //        if (String.IsNullOrWhiteSpace(this.DateOutputMask) || String.IsNullOrWhiteSpace(this.TimeOutputMask)) return null;
        //        return this.DateOutputMask + " " + this.TimeOutputMask;
        //    }
        //}

        //public string DateMaskHint = "dd/mm/yyyy";
        //public string netDateMaskChart = "dd/MM/yyyy";

        //public string CurrencySymbol = "$";
        //public string PercentSymbol = "%";
        //public string DecimalSymbol = ".";
        //public string ThousandGroupSeparator = ",";
        //public bool UserNameStyleUSA = false;

        //public CommonTypes.ShowRegionInAddress? ShowRegionInAddress = null;
        //public bool ShowZipCodeInAddress = false;

        //public CountrySettings() : base() { }
        internal void CopyFrom(game.myGame.sysCountrySettingsRow row)
        {
            this.Country = row.country.Trim();
            this.PostCode = row.postCode.Trim();
            this.CultureCode = row.cultureCode.Trim();

            //this.webAmtMask = row.webAmtMask.Trim();
            //this.netAmtMask = row.netAmtMask.Trim();

            //this.webPhoneMask = row.webPhoneMask.Trim();
            //this.netPhoneMask = row.netPhoneMask.Trim();

            //this.DateInputMask = row.dateInputMask.Trim();
            //this.TimeInputMask = row.timeInputMask.Trim();

            //this.DateOutputMask = row.dateOutputMask.Trim();
            //this.TimeOutputMask = row.timeOutputMask.Trim();

            //this.netDateMaskChart = row.netDateMaskChart.Trim();

            //this.DateMaskHint = row.dateMaskHint.Trim();

            //this.CurrencySymbol = row.currencySymbol.Trim();
            //this.PercentSymbol = row.percentSymbol.Trim();
            //this.DecimalSymbol = row.decimalSymbol.Trim();
            //this.ThousandGroupSeparator = row.thousandGroupSeparator.Trim();

            //this.ShowRegionInAddress = (row.IsshowRegionInAddrNull() ? CommonTypes.ShowRegionInAddress.None : (CommonTypes.ShowRegionInAddress)row.showRegionInAddr);
            //this.ShowZipCodeInAddress = row.showZipcodeInAddr;
            //this.UserNameStyleUSA = row.userNameStyleUSA;
        }

        ///// <summary>
        ///// Load country settings : phone /date / number formats
        ///// </summary>
        ///// <param name="code"></param>
        ///// <returns></returns>
        //public bool LoadByCulture(string code)
        //{
        //    try
        //    {
        //        if (string.IsNullOrWhiteSpace(code)) return false;
        //        data.systemDS.sysCountrySettingsDataTable dataTbl = DataAccess.DbCache.CountrySettings;
        //        if (dataTbl == null)
        //        {
        //            CommonTypes.SysLibs.WriteErrorLog("LoadByCulture FAILED");
        //            return false;
        //        }
        //        DataView dataView = new DataView(dataTbl);
        //        dataView.RowFilter = String.Format("{0}='{1}'", dataTbl.cultureCodeColumn.ColumnName, code);
        //        lock (DataAccess.DbCache.mySystemDS)
        //        {
        //            if (dataView.Count == 0) return false;
        //            this.CopyFrom((data.systemDS.sysCountrySettingsRow)dataView[0].Row);
        //            return true;
        //        }
        //    }
        //    catch (Exception er)
        //    {
        //        CommonTypes.SysLibs.WriteErrorLog(er);
        //        return false;
        //    }
        //}

        /// <summary>
        /// Load country settings : phone /date / number formats
        /// </summary>
        /// <param name="code">Country code in ISO 3166-1 alpha-2 </param>
        /// <returns></returns>
        public bool LoadByCountry(string code)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code)) return false;
                game.myGame.sysCountrySettingsDataTable dataTbl = DataAccess.DbCache.CountrySettings;
                if (dataTbl == null)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog("LoadByCountry FAILED");
                    return false;
                }
                DataView dataView = new DataView(dataTbl);
                dataView.RowFilter = String.Format("{0}='{1}'", dataTbl.countryColumn.ColumnName, code);
                lock (DataAccess.DbCache.myGameDS)
                {
                    if (dataView.Count == 0) return false;
                    this.CopyFrom((game.myGame.sysCountrySettingsRow)dataView[0].Row);
                    return true;
                }
            }
            catch (Exception er)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(er);
                return false;
            }
        }

        public bool LoadByPostCode(string code)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code)) return false;
                game.myGame.sysCountrySettingsDataTable dataTbl = DataAccess.DbCache.CountrySettings;
                if (dataTbl == null)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog("LoadByPostCode FAILED");
                    return false;
                }
                DataView dataView = new DataView(dataTbl);
                dataView.RowFilter = String.Format("{0}='{1}'", dataTbl.postCodeColumn.ColumnName, code);
                lock (dataTbl)
                {
                    if (dataView.Count == 0) return false;
                    this.CopyFrom((game.myGame.sysCountrySettingsRow)dataView[0].Row);
                    return true;
                }
            }
            catch (Exception er)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(er);
                return false;
            }
        }

        //public bool LoadByLanguage(CommonTypes.SysLanguage lang)
        //{
        //    return LoadByCulture(CommonTypes.SysLibs.GetCultureCode(lang));
        //}
    }

    public static partial class AppLibs {
        /// <summary>
        /// Make login id from phone number
        /// </summary>
        /// <param name="country"></param>
        /// <param name="phoneNo"></param>
        /// <returns></returns>
        internal static string MakeLoginId(string country, string phoneNo,bool withFormat = false)
        {
            if (string.IsNullOrWhiteSpace(phoneNo)) return phoneNo;
            string postCode = GetPostCode(country);
            if (postCode == null) return phoneNo;

            string shortPhoneNo = Regex.Replace(phoneNo, "[^.0-9]", string.Empty);
            //Strip off "0" at the begining of phone no
            if (shortPhoneNo.StartsWith("0")) shortPhoneNo = shortPhoneNo.Substring(1);

            if (shortPhoneNo.StartsWith(postCode)) shortPhoneNo = shortPhoneNo.Substring(postCode.Length);
            else if (shortPhoneNo.StartsWith(postCode + "0") || shortPhoneNo.StartsWith("0" + postCode)) shortPhoneNo = shortPhoneNo.Substring(postCode.Length + 1);

            if (withFormat == false) return postCode + shortPhoneNo;
            return FormatPhoneNo(country, shortPhoneNo);
        }

        internal static string GetPostCode(string country)
        {
            if (string.IsNullOrWhiteSpace(country)) return null;
            CountrySettings settings = new CountrySettings();
            if (settings.LoadByCountry(country) == false) return null;
            if (string.IsNullOrWhiteSpace(settings.PostCode)) return null;
            return settings.PostCode.Trim();
        }

        /// <summary>
        /// Format a phone string using country's phone mask. For example "(800) 555-1212"
        /// </summary>
        /// <param name="country"></param>
        /// <param name="shortPhoneNo"></param>
        /// <returns></returns>
        internal static string FormatPhoneNo(string country, string shortPhoneNo)
        {
            if (string.IsNullOrWhiteSpace(shortPhoneNo)) return null;
            if (string.IsNullOrWhiteSpace(country)) return null;
            CountrySettings settings = new CountrySettings();
            if (settings.LoadByCountry(country) == false) return null;
            if (string.IsNullOrWhiteSpace(settings.PostCode)) return null;

            long number;
            //Split post code/phone no mask
            string[] maskItems = settings.netPhoneMask.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            string postCodeMask = "##", phoneNoMask = Game.Consts.DefaultPhoneFormat;
            switch (maskItems.Length)
            {
                case 1: phoneNoMask = maskItems[0]; break;
                case 2: postCodeMask = maskItems[0]; phoneNoMask = maskItems[1]; break;
            }
            string postCode = (long.TryParse(settings.PostCode, out number) ? String.Format("{0:" + postCodeMask + "}", number) : settings.PostCode);

            //Remove all non-digit character
            shortPhoneNo = Regex.Replace(shortPhoneNo, "[^.0-9]", string.Empty);
            return postCode + CommonTypes.Transform.FormatR2L(shortPhoneNo, phoneNoMask, '#');
        }
    }
}

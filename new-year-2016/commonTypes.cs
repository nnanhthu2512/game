using System;
using System.Collections.Specialized;
using System.Xml;
using System.Globalization;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using CommonLibs;
using Game;

namespace CommonTypes
{
    public enum NetProtocol : byte
    {
        None = 0, HTTP = 1, HTTPS = 2
    }

    //Folder under web root folder + Consts.FolderApplication
    public enum FolderTypes : byte
    {
        System,
        GameData
    };

    //Folder under FolderTypes.System folder
    public enum SystemFileType : byte
    {
        Game,
    };

    //Folder under FolderTypes.System folder
    public enum SystemFolderType : byte
    {
        Data,
        Image
    };
    public enum SystemFolderImg : byte
    {
        Background, Gift, Logo,
    }
    public enum SignatureImportance : byte
    {
        None = 0, Low = 1, Average = 2, High = 3, Forever = 4
    };
    public enum SysLanguage : byte
    {
        English = 0, Vietnam = 1, Thailand = 2, Chinese = 3, French = 4, Russia = 5
    };

    /// <summary>
    /// Hash algorithms
    /// http://searchvb.techtarget.com/tip/0,289483,sid8_gci1223163,00.html
    /// </summary>
    public enum HashType : int
    {
        SHA1,
        SHA256,
        SHA384,
        SHA512,
        MD5,
        RIPEMD160
    }

    public enum CacheType : byte { Auto = 0, System = 1, WebCache = 2, Redis = 4, MemCache = 8 }

    public enum ApiStatusCode : byte
    {
        OK = 0, DataNotCompleted = 1, Existed = 4, Expired = 5, OutOfData = 6, Rejected = 7,
        UnAuthorized = 8, NotFound = 9, Pending = 10, Locked = 11, Error = 254
    }

    public static class Config
    {
        static Config()
        {
            appConfigFile.UseEncryption = false;
        }

        private static CommonConfig appConfigFile = new CommonConfig(GetFile());

        private static bool GetConfig(string node, ref StringCollection aFields)
        {
            return appConfigFile.GetConfig(node, null, aFields);
        }

        private static string GetFile()
        {
            string temp = CommonLibs.FileSystem.Combine(CommonLibs.FileSystem.GetRootFolderPath(), Game.Consts.ApplicationConf);
            //CommonLibs.ErrorLibs.WriteDebugLog("file: " + temp);
            return temp;
        }

        internal static void Load()
        {
            try
            {
                LoadAPPLICATION();
            }
            catch (Exception er)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(er);
            }
        }

        private static bool SetValue(ref string[] list, string str)
        {
            if (String.IsNullOrWhiteSpace(str) == false)
            {
                string[] newList = str.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                list = new string[newList.Length];
                newList.CopyTo(list, 0);
                return true;
            }
            return false;
        }

        private static bool SetValue(StringCollection list, string str)
        {
            string[] tmpList = new string[0];
            SetValue(ref tmpList, str);
            list.Clear();
            list.AddRange(tmpList);
            return true;
        }

        private static void LoadAPPLICATION(string configKey = "Application")
        {
            StringCollection aFields = new StringCollection();
            aFields.Clear();
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.MinTotalGifts }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.MaxTotalGifts }));

            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.WidthGiftSceneDefault }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.HeightGiftSceneDefault }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.MinGiftSize }));

            //Use cloud
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.SysUseCloudStorage }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.SysUseCloudStorageContainer }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.SysUseCloudPublicContainer }));

            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.GdrApiURL }));

            //Memcache
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.sysMemcacheServerAddr }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.sysMemcacheServerPort }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.sysMemcacheTimeInSecs }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.sysMemcacheEnabled }));

            //data file name
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.SceneFileName }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.GiftFileName }));
            aFields.Add(CommonLibs.SysLibs.GetName(new { Settings.SpotFileName }));


            if (GetConfig(configKey, ref aFields) == false) return;
            ushort valShort = 0; bool valBool = false; double valDouble = 0;

            int count = -1;
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.MinTotalGifts = valShort;
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.MaxTotalGifts = valShort;
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.WidthGiftSceneDefault = valShort;
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.HeightGiftSceneDefault = valShort;
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.MinGiftSize = valShort;

            if (bool.TryParse(aFields[++count], out valBool)) Settings.SysUseCloudStorage = valBool;
            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) Settings.SysUseCloudStorageContainer = aFields[count].ToLower();
            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) Settings.SysUseCloudPublicContainer = aFields[count].ToLower();

            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) Settings.GdrApiURL = aFields[count].ToLower();

            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) SetValue(Settings.sysMemcacheServerAddr, aFields[count]);
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.sysMemcacheServerPort = valShort;
            if (ushort.TryParse(aFields[++count], out valShort)) Settings.sysMemcacheTimeInSecs = valShort;
            if (bool.TryParse(aFields[++count], out valBool)) Settings.sysMemcacheEnabled = valBool;

            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) Settings.SceneFileName = aFields[count].ToLower();
            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) Settings.GiftFileName = aFields[count].ToLower();
            if (String.IsNullOrWhiteSpace(aFields[++count]) == false) Settings.SpotFileName = aFields[count].ToLower();
        }

    }

    [Serializable()]
    public class BaseObject : ICloneable
    {
        public BaseObject()
        {
        }

        public virtual bool Validate()
        {
            return true;
        }

        public virtual object Clone()
        {
            //return OX.Copyable.ObjectExtensions.Copy(this);
            return (BaseObject)MemberwiseClone();
        }
    }

    /// <summary>
    /// Prepresent a token storing a key that only valid in an amount of time
    /// </summary>
    [Serializable()]
    internal class Token : BaseObject
    {
        public string Key { get; set; }
        public object Value { get; set; }
        public DateTime? ExpiredOn { get; set; }
        public bool IsExpired()
        {
            if (this.ExpiredOn == null) return false;
            return this.ExpiredOn < DateTime.UtcNow;
        }
    }

    public static class Transform
    {
        static List<TextValue> myEncodePairs = new List<TextValue>();
        static Transform()
        {
            myEncodePairs.Add(new TextValue(@"%", @"(_a_)"));
            myEncodePairs.Add(new TextValue(@"&", @"(_b_)"));
            myEncodePairs.Add(new TextValue(@"/", @"(_c_)"));
            myEncodePairs.Add(new TextValue(@"+", @"(_d_)"));
            myEncodePairs.Add(new TextValue(@"-", @"(_e_)"));
        }

        /// <summary>
        /// Format a string using the specified mask (starting from right to left)
        /// </summary>
        /// <param name="inputStr"></param>
        /// <param name="mask"></param>
        /// <param name="maskMarker">Character mask the meaningfull character in the input string</param>
        /// <returns>Null if error</returns>
        public static string FormatR2L(string inputStr, string mask, char maskMarker)
        {
            string newPhoneNo = "";
            int cutPos = inputStr.Length - 1;
            for (int idx = mask.Length - 1; idx >= 0; idx--)
            {
                string tmp = mask.Substring(idx, 1);
                if (tmp[0] == maskMarker)
                {
                    if (cutPos >= 0)
                    {
                        newPhoneNo = inputStr[cutPos] + newPhoneNo;
                        cutPos--;
                    }
                    else break;
                }
                else newPhoneNo = tmp + newPhoneNo;
            }
            //Add the remaining if any
            if (cutPos >= 0) newPhoneNo = inputStr.Substring(0, cutPos + 1) + newPhoneNo;
            return newPhoneNo;
        }

        /// <summary>
        /// Seriallize object to string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>return base64String</returns>
        public static string Obj2Str(object obj)
        {
            if (obj == null) return null;
            using (MemoryStream ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, obj);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        /// <summary>
        /// Deseriallize a string to object
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static object Str2Obj(string base64String)
        {

            byte[] bytes = Convert.FromBase64String(base64String);
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                return new BinaryFormatter().Deserialize(ms);
            }
        }

        /// <summary>
        /// Convert string to decimal
        /// </summary>
        /// <param name="str"></param>
        /// <param name="lang"></param>
        /// <param name="numStyle"></param>
        /// <returns></returns>
        public static decimal? Str2Decimal(string str, SysLanguage lang, NumberStyles numStyle = NumberStyles.Any)
        {
            if (String.IsNullOrWhiteSpace(str)) return null;
            decimal num = 0;
            if (lang == SysLanguage.Vietnam) str = str.Replace('.', ',');
            else str = str.Replace(',', '.');
            if (!decimal.TryParse(str, numStyle, SysLibs.GetLanguageInfo(lang).Culture, out num))
            {
                return null;
            }
            return num;
        }

        ///// <summary>
        ///// Encode a string so that it can be transfered in HTTP environment
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //public static string EncodeUrlQueryString(string str)
        //{
        //    return Transform.ToHexString(str);
        //}

        ///// <summary>
        ///// Convert a string to hex-string
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //internal static string ToHexString(string str)
        //{
        //    string retStr = "";
        //    foreach (byte t in str) retStr += t.ToString("X2");
        //    return retStr;
        //}

        ///// <summary>
        ///// Decode a string which was encoded by EncodeUrlQueryString()
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //public static string DecodeUrlQueryString(string str)
        //{
        //    return Transform.FromHexString(str);
        //}

        ///// <summary>
        ///// Convert a hex-string back to original string
        ///// </summary>
        ///// <param name="hexString"></param>
        ///// <returns></returns>
        //internal static string FromHexString(string hexString)
        //{
        //    string retStr = "";
        //    for (var i = 0; i < hexString.Length; i += 2)
        //    {
        //        retStr += (char)Convert.ToInt16(hexString.Substring(i, 2), 16);
        //    }
        //    return retStr;
        //}
    }

    /// <summary>
    /// Data structure for storing tokens
    /// </summary>
    internal class TokenTbl : DataTable
    {
        /// <summary>
        /// The system will automatically fire "cleaning process" when total operations (add,remove...) reachs this setting.
        /// RemoveExpire() perfoms the "cleaning process"  by removing expired items from the data
        /// </summary>
        internal int SetCleanCount = 1000;
        int CleanCount = 0;

        DataView TokenByExpire = new DataView();
        public TokenTbl() : base()
        {
            this.TableName = "TokenTbl";
            this.Columns.Add("key", typeof(string));
            this.Columns.Add("value", typeof(string));
            this.Columns.Add("expiredOn", typeof(DateTime));

            this.PrimaryKey = new DataColumn[] { this.Columns["Key"] };

            TokenByExpire.Table = this;
            TokenByExpire.Sort = "expiredOn DESC";
        }

        /// <summary>
        /// TNeed to remove expired token
        /// </summary>
        private void Cleaning()
        {
            if (this.CleanCount > 0) this.CleanCount--;
            else
            {
                RemoveExpire();
                this.CleanCount = this.SetCleanCount;
            }
        }

        public virtual bool Add(Token token)
        {
            if (token == null || token.Key == null) return false;
            DataRow row = this.GetData(token.Key);

            lock (this)
            {
                if (row != null) Copy(token, row);
                else this.Rows.Add(token.Key, Transform.Obj2Str(token.Value), token.ExpiredOn);
                return true;
            }
        }

        public virtual bool AddNew(Token token)
        {
            if (token == null) return false;
            if (this.GetData(token.Key) != null) return false;
            lock (this)
            {
                this.Rows.Add(token.Key, Transform.Obj2Str(token.Value), token.ExpiredOn);
                return true;
            }
        }

        public virtual bool Update(Token token)
        {
            if (token == null) return false;
            DataRow row = this.GetData(token.Key);
            if (row == null) return false;
            lock (this)
            {
                Copy(token, row);
                return true;
            }
        }
        public virtual bool Remove(string key)
        {
            if (String.IsNullOrWhiteSpace(key)) return false;
            this.Cleaning();

            DataRow row = this.Rows.Find(key);
            if (row == null) return false;
            lock (this)
            {
                row.Delete();
                return true;
            }
        }

        public virtual Token Get(string key)
        {
            DataRow row = this.GetData(key);
            if (row == null) return null;
            Token token = new Token();
            Copy(row, token);
            if (token.IsExpired())
            {
                this.Remove(key);
                return null;
            }
            return token;
        }

        internal virtual Token Get(int idx)
        {
            if (idx >= 0 && idx < this.Rows.Count)
            {
                Token token = new Token();
                Copy(this.Rows[idx], token);
                if (token.IsExpired())
                {
                    lock (this)
                    {
                        this.Rows[idx].Delete();
                        return null;
                    }
                }
                return token;
            }
            return null;
        }

        internal virtual void Copy(DataRow row, Token token)
        {
            token.Key = row["key"].ToString();
            token.Value = (row["value"] == DBNull.Value ? null : Transform.Str2Obj(row["value"].ToString()));
            token.ExpiredOn = (row["expiredOn"] == DBNull.Value ? null : (DateTime?)row["expiredOn"]);
        }
        internal virtual void Copy(Token token, DataRow row)
        {
            row["key"] = token.Key;
            row["value"] = (token.Value == null ? null : Transform.Obj2Str(token.Value));
            row["expiredOn"] = token.ExpiredOn;
        }
        internal virtual DataRow GetData(string key)
        {
            if (String.IsNullOrWhiteSpace(key)) return null;
            this.Cleaning();

            return this.Rows.Find(key);
        }

        /// <summary>
        /// Remove expited items
        /// </summary>
        internal virtual void RemoveExpire()
        {
            int count1 = TokenByExpire.Count;
            lock (this.TokenByExpire)
            {
                Token token = new Token();
                DateTime curTime = DateTime.UtcNow;
                while (TokenByExpire.Count > 0)
                {
                    Copy(TokenByExpire[0].Row, token);
                    if (token.IsExpired())
                    {
                        TokenByExpire[0].Delete();
                        continue;
                    }
                    break;
                }
            }
            if (Settings.sysDebugMode && count1 != TokenByExpire.Count)
                ErrorLibs.WriteLog(String.Format("RemoveExpire {0} items from {1} to {2}", count1 - TokenByExpire.Count, count1, TokenByExpire.Count));
        }
    }

    internal class CommonConfig
    {
        string myConfFile = null;
        public CommonConfig(string confFile = null)
        {
            this.myConfFile = confFile != null ? confFile : Settings.sysUserConfigFile;
            myConfigXmlDoc = CommonLibs.xmlLibs.OpenXML(this.myConfFile);
        }
        public bool UseEncryption = true;
        private XmlDocument myConfigXmlDoc = null;

        private string[] GetNodeNames(string type, string subType)
        {
            if (CommonLibs.StringLibs.IsNullOrWhiteSpace(subType))
            {
                if (CommonLibs.StringLibs.IsNullOrWhiteSpace(type))
                    return new string[] { Game.Consts.constXmlRootElement };
                return new string[] { Game.Consts.constXmlRootElement, type };
            }
            return new string[] { Game.Consts.constXmlRootElement, type, subType };
        }

        public bool GetConfig(string type, string subType, StringCollection aFields)
        {
            return GetConfig(GetNodeNames(type, subType), aFields);
        }

        internal bool GetConfig(string[] nodes, StringCollection aFields)
        {
            string xmlPath = CommonLibs.xmlLibs.MakeXmlPath(nodes);
            bool retVal = true;
            for (int idx = 0; idx < aFields.Count; idx++)
            {
                aFields[idx] = CommonLibs.xmlLibs.GetElement(myConfigXmlDoc, xmlPath, aFields[idx], UseEncryption);
                if (aFields[idx] == null)
                {
                    aFields[idx] = "";
                }
            }
            return retVal;
        }
    }

    public class LanguageInfo
    {
        public LanguageInfo(SysLanguage lang)
        {
            this.Lang = lang;
            //The second param must be FALSE to ensure that 
            //the culture properties are independent of user settings (in ControlPanel)
            this.Culture = new CultureInfo(GetCultureCode(), false);
        }
        string GetCultureCode()
        {
            switch (this.Lang)
            {
                case SysLanguage.Chinese: return "zh-CN";
                case SysLanguage.Vietnam: return "vi-VN";
                case SysLanguage.French: return "fr-FR";
                case SysLanguage.Russia: return "ru-RU";
                case SysLanguage.Thailand: return "th-TH";
                default: return "en-US";
            }
        }

        internal SysLanguage Lang;
        public CultureInfo Culture;
    }

    internal class TextValue
    {
        private string _text = null;
        private string _value = null;
        private object _tag = null;

        public TextValue() { }
        public TextValue(string text, string value)
        {
            _text = text;
            _value = value;
        }
        public TextValue(string text, string value, object tag)
        {
            _text = text;
            _value = value;
            _tag = tag;
        }
        //The item will display in the combo box based on how you implemented
        //ToString(). In this case, the name is displayed. But, when the object is
        //selected, you have this object, which may contain as much data as you need.
        public override string ToString()
        {
            return _text;
        }
        public virtual string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public virtual string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        public virtual object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }
    }

    [Serializable()]
    /// <summary>
    /// Represent a point (Longitude,Latitude) on Geo map
    /// </summary>
    public class LongLat
    {
        public decimal? Latitude = null, Longitude = null; // In Degrees
        public LongLat()
        {
            this.SetNull();
        }
        public LongLat(decimal? longitude, decimal? latitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
        public bool IsNull()
        {
            return this.Latitude == null || this.Longitude == null;
        }
        public void SetNull()
        {
            this.Latitude = null;
            this.Longitude = null;
        }
        public void SetZero()
        {
            this.Latitude = 0;
            this.Longitude = 0;
        }
        public void SetBy(LongLat info, LongLat maskFlds = null)
        {
            if (info == null)
            {
                this.SetNull();
            }
            else
            {
                if (maskFlds == null || maskFlds.Latitude != null) this.Latitude = info.Latitude;
                if (maskFlds == null || maskFlds.Longitude != null) this.Longitude = info.Longitude;
            }
        }

        public bool IsEqual(LongLat info)
        {
            if (info == null) return false;
            return (this.Latitude == info.Latitude) && (this.Longitude == info.Longitude);
        }

        public override string ToString()
        {
            return string.Format("{0:0000.0000000000}-{1:0000.0000000000}", this.Latitude, this.Longitude);
        }
    }

    [Serializable()]
    public class Location : BaseObject
    {
        internal virtual string StreetNumber { get; set; }
        internal virtual string StreetName { get; set; }

        public virtual string City { get; set; }
        public virtual string Region { get; set; }
        internal virtual string SubLocalLevel1 { get; set; }  //District
        internal virtual string SubLocalLevel2 { get; set; }  //Ward
        internal virtual string SubLocalLevel3 { get; set; }  //Hamlet

        public virtual string Country { get; set; }
        public virtual string ZipCode { get; set; }

        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual void Reset()
        {
            this.Country = null;
            this.Region = null;
            this.City = null;
            this.ZipCode = null;
            this.Address1 = null;
            this.Address2 = null;
            this.SubLocalLevel1 = null;
            this.SubLocalLevel2 = null;
            this.SubLocalLevel3 = null;
        }
        public virtual void SetAll()
        {
            this.Country = "";
            this.Region = "";
            this.City = "";
            this.ZipCode = "";
            this.Address1 = "";
            this.Address2 = "";
            this.SubLocalLevel1 = "";
            this.SubLocalLevel2 = "";
            this.SubLocalLevel3 = "";
        }
        public override bool Validate()
        {
            this.Country = (String.IsNullOrWhiteSpace(this.Country) ? null : this.Country.Trim());
            this.Region = (String.IsNullOrWhiteSpace(this.Region) ? null : this.Region.Trim());
            this.City = (String.IsNullOrWhiteSpace(this.City) ? null : this.City.Trim());

            this.ZipCode = (String.IsNullOrWhiteSpace(this.ZipCode) ? null : this.ZipCode.Trim());
            this.Address1 = (String.IsNullOrWhiteSpace(this.Address1) ? null : this.Address1.Trim());
            this.Address2 = (String.IsNullOrWhiteSpace(this.Address2) ? null : this.Address1.Trim());
            return true;
        }
        public Location() : base()
        {
            this.Reset();
        }
        public void CopyFrom(Location info, Location maskFlds = null)
        {
            if (maskFlds == null || maskFlds.StreetNumber != null) this.StreetNumber = info.StreetNumber;
            if (maskFlds == null || maskFlds.StreetName != null) this.StreetName = info.StreetName;
            if (maskFlds == null || maskFlds.SubLocalLevel1 != null) this.SubLocalLevel1 = info.SubLocalLevel1;
            if (maskFlds == null || maskFlds.SubLocalLevel2 != null) this.SubLocalLevel2 = info.SubLocalLevel2;
            if (maskFlds == null || maskFlds.SubLocalLevel3 != null) this.SubLocalLevel3 = info.SubLocalLevel3;

            if (maskFlds == null || maskFlds.StreetNumber != null) this.StreetNumber = info.StreetNumber;
            if (maskFlds == null || maskFlds.Country != null) this.Country = info.Country;
            if (maskFlds == null || maskFlds.Region != null) this.Region = info.Region;
            if (maskFlds == null || maskFlds.City != null) this.City = info.City;
            if (maskFlds == null || maskFlds.Address1 != null) this.Address1 = info.Address1;
            if (maskFlds == null || maskFlds.Address2 != null) this.Address2 = info.Address2;
            if (maskFlds == null || maskFlds.ZipCode != null) this.ZipCode = info.ZipCode;
        }
    }

    [Serializable()]
    public class LocationAndLongLat : Location
    {
        public LongLat LongLat = new LongLat();
        public override void Reset()
        {
            base.Reset();
            this.LongLat.SetNull();
        }
        public override void SetAll()
        {
            base.SetAll();
            this.LongLat.SetZero();
        }
        public void CopyFrom(LocationAndLongLat info, LocationAndLongLat maskFlds = null)
        {
            base.CopyFrom(info, maskFlds);
            if (maskFlds != null && maskFlds.LongLat != null) this.LongLat.SetBy(info.LongLat, maskFlds.LongLat);
        }
    }

    [Serializable()]
    /// <summary>
    /// Represent geo info collected from an IP address
    /// </summary>
    public class BaseGeoIP
    {
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public void CopyFrom(BaseGeoIP geo)
        {
            this.Ip = geo.Ip;
            this.CountryCode = geo.CountryCode;
            this.CountryName = geo.CountryName;
        }
    }

    [Serializable()]
    public class GeoIP : BaseGeoIP
    {
        public string RegionCode { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MetroCode { get; set; }
        public string AreaCode { get; set; }
        public string TimeZone { get; set; }

        public void CopyFrom(GeoIP geo)
        {
            base.CopyFrom((BaseGeoIP)geo);
            this.CountryName = geo.CountryName;
            this.RegionCode = geo.RegionCode;
            this.City = geo.City;
            this.ZipCode = geo.ZipCode;
            this.Latitude = geo.Latitude;
            this.Longitude = geo.Longitude;
            this.MetroCode = geo.MetroCode;
            this.AreaCode = geo.AreaCode;
        }
    }
}
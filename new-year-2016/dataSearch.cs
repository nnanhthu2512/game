﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataSearch.cs 								                    */
/* Description : Classes and functions to retrieve/search data by               */
/* criteria designed for specific data objects                                  */
/* Developers : Dung Vu , Vietnam                                               */
/*													                            */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using Game;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
    [Serializable()]
    public class EngineInfo : CommonTypes.BaseObject
    {
        internal bool UseUnicode = true;
        private FieldOptionList FieldOptions = new FieldOptionList();
        private JoinOptionList JoinOptions = new JoinOptionList();

        private List<TblNames> JoinedTables = new List<TblNames>();
        private List<TblNames> MustJoinTables = new List<TblNames>();
        private List<TblNames> NotJoinTables = new List<TblNames>();

        internal void AddJoinOptions(TblNames tn, JoinTypes type)
        {
            this.JoinOptions.Add(tn, type);
        }
        internal void AddFieldOptions(TblNames tn, string fld, FieldOptionItem value)
        {
            this.FieldOptions.Add(tn, fld, value);
        }

        internal bool JoinedContain(TblNames tn)
        {
            return this.JoinedTables.Contains(tn);
        }
        internal bool MustJoinContain(TblNames tn)
        {
            return this.MustJoinTables.Contains(tn);
        }
        internal bool NotJoinContain(TblNames tn)
        {
            return this.NotJoinTables.Contains(tn);
        }

        internal void AddJoined(TblNames tn)
        {
            if (this.JoinedTables.Contains(tn) == false) this.JoinedTables.Add(tn);
        }
        internal void AddMustJoin(TblNames tn)
        {
            if (this.MustJoinTables.Contains(tn) == false) this.MustJoinTables.Add(tn);
            //Remove table name in joined list to ensure that the mustjoin list will take effect
            if (this.JoinedTables.Contains(tn)) this.JoinedTables.Remove(tn);
        }
        internal void AddNotJoin(TblNames tn)
        {
            if (this.NotJoinTables.Contains(tn) == false) this.NotJoinTables.Add(tn);
        }

        internal void RemoveMustJoin(TblNames tn)
        {
            if (this.MustJoinTables.Contains(tn)) this.MustJoinTables.Remove(tn);
        }
        internal void RemoveJoined(TblNames tn)
        {
            if (this.JoinedTables.Contains(tn)) this.JoinedTables.Remove(tn);
        }
        internal void RemoveNotJoined(TblNames tn)
        {
            if (this.NotJoinTables.Contains(tn)) this.NotJoinTables.Remove(tn);
        }

        internal SqlCommand SqlCommand = new SqlCommand();
        internal string JoinCmd = "", FilterCmd = "", HavingFilterCmd = "", GroupByCmd = "", OrderByCmd = "";

        internal virtual void ClearAll()
        {
            this.Reset(true);
            this.FieldOptions.Clear();
        }

        internal virtual void Reset(bool keepParam=false)
        {
            this.FilterCmd = "";
            this.JoinCmd = "";
            this.HavingFilterCmd = "";
            this.GroupByCmd = "";
            this.OrderByCmd = "";
            this.UseUnicode = true;
            
            this.JoinOptions.Clear();
            this.JoinedTables.Clear();
            if (keepParam==false) this.SqlCommand.Parameters.Clear();
        }

        //Return True : must join the specified table. Nalse Not join. Null : Not specified and will be decided by calling funcs
        internal bool? MustMakeJoin(TblNames tn)
        {
            if (this.JoinedContain(tn)) return false;
            if (this.NotJoinTables.Contains(tn)) return false;
            if (this.MustJoinContain(tn)) return true;
            return null;
        }

        internal string GetNewParam(string varSuffix = null)
        {
            return EngineLibs.GetNewParam(this.SqlCommand, varSuffix);
        }
        internal void AddSqlParam(string varName, object value, SqlDbType? type = null)
        {
            EngineLibs.AddSqlParam(this.SqlCommand, varName, value, type);
        }

        internal SearchOptions GetSearchOption(TblNames tn, FldNames fn, SearchOptions  ifNullOption)
        {
            if (this.FieldOptions.ContainsKey(tn, fn.ToString())) return this.FieldOptions.GetValue(tn, fn.ToString()).SearchOption;
            return ifNullOption;
        }
        internal SearchOptions GetSearchOption(TblNames tn, string fn, SearchOptions  ifNullOption)
        {
            if (this.FieldOptions.ContainsKey(tn, fn)) return this.FieldOptions.GetValue(tn, fn).SearchOption;
            return ifNullOption;
        }

        internal JoinTypes GetJoinType(TblNames tn, JoinTypes ifNull)
        {
            if (this.JoinOptions.ContainsKey(tn)) return this.JoinOptions[tn];
            return ifNull;
        }

        internal string BuildJoinCmd(TblNames fromTable, FldNames fromFld, JoinTypes defaultJoinType, TblNames toTable, FldNames toFld)
        {
            string tmp = String.Format("{0} {1} ON {2}={3}",
                                EngineLibs.GetTableName(toTable), EngineLibs.GetTableAlias(toTable),
                                EngineLibs.GetFieldName(fromTable, fromFld), EngineLibs.GetFieldName(toTable, toFld));
            return EngineLibs.GetJoinTypeCmd(this.GetJoinType(toTable, defaultJoinType)) + " " + tmp;
        }

        internal string BuildJoinCmd(TblNames fromTable, string[] fromFlds, JoinTypes defaultJoinType, TblNames toTable, string[] toFlds)
        {
            if (fromFlds.Length != toFlds.Length) return null;

            string fromAlias = EngineLibs.GetTableAlias(fromTable);
            string toAlias = EngineLibs.GetTableAlias(toTable);
            string joinCmd = "";
            for (int idx = 0; idx < fromFlds.Length; idx++)
            {
                joinCmd += (joinCmd == "" ? "" : " AND ") + String.Format("{0}.{1}={2}.{3}", fromAlias, fromFlds[idx], toAlias, toFlds[idx]);
            }
            if (String.IsNullOrWhiteSpace(joinCmd)) return null;

            joinCmd = String.Format("{0} {1}", EngineLibs.GetTableName(toTable), toAlias) + " ON " + joinCmd;

            return EngineLibs.GetJoinTypeCmd(this.GetJoinType(toTable, defaultJoinType)) + " " + joinCmd;
        }
        internal string BuildJoinCmd(TblNames fromTable, string fromFld, JoinTypes defaultJoinType, TblNames toTable, string toFld)
        {
            return BuildJoinCmd(fromTable, new string[] { fromFld }, defaultJoinType, toTable, new string[] { toFld });
        }

        internal void MakeExpression(string searchBy, string searchTerm, string varName, SearchOptions option)
        {
            string filter = EngineLibs.BuildFilter(this.SqlCommand, searchBy, searchTerm, varName, option);
            if (String.IsNullOrWhiteSpace(filter) == false)
                this.FilterCmd += (String.IsNullOrWhiteSpace(this.FilterCmd) ? "" : " AND ") + filter;
        }

        internal void AddFilter(string filter, string opertator="AND")
        {
            if (String.IsNullOrWhiteSpace(filter)) return;
            this.FilterCmd = (String.IsNullOrWhiteSpace(this.FilterCmd) ? "" : this.FilterCmd + " " + opertator + " ") + "(" + filter +")";
        }
        internal void AddJoin(string joinCmd)
        {
            if (String.IsNullOrWhiteSpace(joinCmd)) return;
            this.JoinCmd = (String.IsNullOrWhiteSpace(this.JoinCmd) ? "" : this.JoinCmd + Game.Consts.constCRLF) + joinCmd;
        }

        internal void AddHavingFilter(string filter, string opertator = "AND")
        {
            if (String.IsNullOrWhiteSpace(filter)) return;
            this.HavingFilterCmd = (String.IsNullOrWhiteSpace(this.HavingFilterCmd) ? "" : this.HavingFilterCmd + " " + opertator + " ") + "(" + filter + ")";
        }
        internal void AddGroupBy(string groupBy)
        {
            if (String.IsNullOrWhiteSpace(groupBy)) return;
            this.GroupByCmd = (String.IsNullOrWhiteSpace(this.GroupByCmd) ? "" : this.GroupByCmd + "," ) + groupBy;
        }
        internal void AddOrderBy(string orderBy)
        {
            if (String.IsNullOrWhiteSpace(orderBy)) return;
            this.OrderByCmd = (String.IsNullOrWhiteSpace(this.OrderByCmd) ? "" : this.OrderByCmd + ",") + orderBy;
        }

        internal void BuildSqlCommandText(string cmd)
        {
            cmd += (String.IsNullOrWhiteSpace(this.JoinCmd) ? "" : Consts.constCRLF + this.JoinCmd) +
                   (String.IsNullOrWhiteSpace(this.FilterCmd) ? "" : Consts.constCRLF + "WHERE " + this.FilterCmd) +
                   (String.IsNullOrWhiteSpace(this.GroupByCmd) ? "" : Consts.constCRLF + "GROUP BY " + this.GroupByCmd) +
                   (String.IsNullOrWhiteSpace(this.HavingFilterCmd) ? "" : Consts.constCRLF + "HAVING " + this.HavingFilterCmd) +
                   (String.IsNullOrWhiteSpace(this.OrderByCmd) ? "" : Consts.constCRLF + "ORDER BY " + this.OrderByCmd);

            this.SetSqlCommandText(cmd);
        }

        internal void SetSqlCommandText(string cmd)
        {
            this.SqlCommand.CommandText = EngineLibs.FinalizeSQL(cmd);
        }
    }
}

/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : libs.cs 								                            */
/* Description : System functions                                               */
/* Developers : Dung Vu , Vietnam                                               */
/* -----------------------------------------------------------------------------*/
/* History 											                            */
/*													                            */
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by :     									                        */
/* Change date :                                                                */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/
using System.Text.RegularExpressions;

namespace CommonTypes
{
    public static class AppLibs
    {
        /// <summary>
        /// Replace accented chars with plain ones, multiple spaces or puntuation are replaced by a single space
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string StandardizeString(string str,
                                               bool removeNonAlphaNumeic = true,
                                               bool removeMultiSpace = true)
        {
            if (str == null) return null;

            str = CommonLibs.StringLibs.ReplaceAccentedText(str);

            if (removeNonAlphaNumeic) str = Regex.Replace(str, "[^A-Za-z0-9 _]", " ");
            if (removeMultiSpace) str = Regex.Replace(str, @"\s+", " ");
            return str;
        }
    }

}

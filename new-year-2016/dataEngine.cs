﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataEngine.cs 								                    */
/* Description : Common classes and functions supporting to retrieve/search     */
/* module.                                                                      */
/*													                            */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Game;

namespace DataAccess
{
    public enum SearchOptions : byte { Exact, StartWith, EndWith, Contain, HaveAll, ftContainAnd, ftContainNear };
    internal enum QueryType : byte { Select=1, Insert=2, Update=4}
    internal enum JoinTypes { Inner, Left, Right, Full, Cross }
    public enum BitOperators { Equal, On, Off };

    internal enum TblNames
    {
        None,
        UserSubmit, Country, SysCountrySettings
    }
    
    internal enum FldNames
    {
        None,
        OrgId, UserId, OwnerId,MemberId,
        Id, FeedbackId, QuestionItemId,
        PostGroupId, ConnectionId
    }

    [Serializable()]
    internal class FieldNameList : SortedList<string, string>
    {
        public void Add(TblNames t, FldNames f, string name)
        {
            if (this.ContainsKey(t.ToString() + "." + f.ToString()))
            {
                this[t.ToString() + "." + f.ToString()] = name;
                return;
            }
            this.Add(t.ToString() + "." + f.ToString(), name);
        }
        public void Remove(TblNames t, FldNames f)
        {
            if (this.ContainsKey(t.ToString() + "." + f.ToString()))
                this.Remove(t.ToString() + "." + f.ToString());
        }
        public string Find(TblNames t, FldNames f)
        {
            if (this.ContainsKey(t.ToString() + "." + f.ToString()))
                return this[t.ToString() + "." + f.ToString()];
            return null;
        }
    }

    [Serializable()]
    internal class TableInfo
    {
        public TableInfo(string tn, string a)
        {
            this.Name = tn;
            this.Alias = a;
        }
        public string Name = "";
        public string Alias = "";
    }

    [Serializable()]
    internal class TableList : SortedList<string, TableInfo>
    {
        StringCollection aliasList = new StringCollection();
        public void Add(string tblName, string dbTblName, string alias)
        {
            this.Add(tblName, new TableInfo(dbTblName, alias));
            if (this.aliasList.Contains(alias) == false) this.aliasList.Add(alias);
        }

        public void Add(TblNames t, string name, string alias)
        {
            this.Add(t.ToString(), new TableInfo(name, alias));
            if (this.aliasList.Contains(alias) == false)
                this.aliasList.Add(alias);
        }
        public string GetName(string key)
        {
            if (this.ContainsKey(key)) return this[key].Name;
            return null;
        }
        public string GetName(TblNames t)
        {
            if (this.ContainsKey(t.ToString())) return this[t.ToString()].Name;
            return null;
        }
        public string GetAlias(TblNames t)
        {
            return this.GetAlias(t.ToString());
        }
        public string GetAlias(string key)
        {
            if (this.ContainsKey(key)) return this[key].Alias;
            return null;
        }
        public new void Clear()
        {
            base.Clear();
            this.aliasList.Clear();
        }

        internal string GetNewAlias(string suffix = null)
        {
            Random ran = new Random();
            string alias = suffix;
            if (String.IsNullOrWhiteSpace(alias))
            {
                alias = Convert.ToChar(ran.Next(Convert.ToInt16('a'), Convert.ToInt16('z'))).ToString();
            }
            while (true)
            {
                alias += ran.Next(0, 9).ToString();
                if (this.aliasList.Contains(alias) == false)
                {
                    this.aliasList.Add(alias);
                    return alias;
                }
            }
        }
    }

    [Serializable()]
    internal class FieldOptionItem
    {
        public SearchOptions SearchOption = SearchOptions.Exact;
    }

    [Serializable()]
    internal class FieldOptionList : SortedList<string, FieldOptionItem>
    {
        public void Add(TblNames tbl, string fld, FieldOptionItem value)
        {
            string key = MakeKey(tbl, fld);
            if (this.ContainsKey(key)) this[key] = value; else base.Add(key, value);
        }

        public bool ContainsKey(TblNames tbl, string fld)
        {
            return this.ContainsKey(MakeKey(tbl, fld));
        }

        public FieldOptionItem GetValue(TblNames tbl, string fld)
        {
            return this[MakeKey(tbl, fld)];
        }

        internal string MakeKey(TblNames tbl, string fldName)
        {
            return String.Format("{0}.{1}", tbl.ToString(), fldName);
        }
    }

    [Serializable()]
    internal class JoinOptionList : SortedList<TblNames, JoinTypes>
    {
        public new void Add(TblNames tbl, JoinTypes value)
        {
            if (this.ContainsKey(tbl))
            {
                this[tbl] = value;
            }
            else
            {
                base.Add(tbl, value);
            }
        }
    }

    public static class EngineLibs
    {
        static FieldNameList myFieldList = new FieldNameList();
        static TableList myTableList = new TableList();

        static EngineLibs()
        {
            UseRetryPolicyIncremental("defaultRetryStrategy");

            myTableList.Add(TblNames.UserSubmit, DbCache.myGameDS.UserSubmit.TableName, "u0");
            myTableList.Add(TblNames.Country, DbCache.myGameDS.country.TableName, "c0");
            myTableList.Add(TblNames.SysCountrySettings, DbCache.myGameDS.sysCountrySettings.TableName, "c1");

        }

        public static void UseRetryPolicyIncremental(string strategyName)
        {
            Incremental myStrategy = new Incremental(strategyName, Settings.sysRetryStrategyCount,
                                                    TimeSpan.FromSeconds(Settings.sysRetryStrategyFirstWaitInSecs),
                                                    TimeSpan.FromSeconds(Settings.sysRetryStrategyIncreaseWaitInSecs));

            RetryPolicy policy = new RetryPolicy<SqlDatabaseTransientErrorDetectionStrategy>(myStrategy);
            policy.Retrying += (obj, eventArgs) =>
            {
                var msg = String.Format("Retrying, CurrentRetryCount = {0} , Delay = {1}, Exception = {2}", eventArgs.CurrentRetryCount, eventArgs.Delay, eventArgs.LastException.Message);
                CommonLibs.ErrorLibs.WriteErrorLog(msg);
            };
            List<RetryStrategy> myStrategies = new List<RetryStrategy> { myStrategy };
            RetryManager myManager = new RetryManager(myStrategies, strategyName);
            RetryManager.SetDefault(myManager, false);
        }

        /// <summary>
        /// Validate params to prevent SQL-Injection
        /// </summary>
        /// <param name="sqlCommand"></param>
        internal static void ValidateParams(SqlCommand sqlCommand)
        {
            for (int idx = 0; idx < sqlCommand.Parameters.Count; idx++)
            {
                if (sqlCommand.Parameters[idx] == null || sqlCommand.Parameters[idx].Value == null || sqlCommand.Parameters[idx].Value == DBNull.Value) continue;
                //Prevent SQL injection
                if (sqlCommand.Parameters[idx].DbType == DbType.String || sqlCommand.Parameters[idx].DbType == DbType.StringFixedLength ||
                    sqlCommand.Parameters[idx].DbType == DbType.AnsiString || sqlCommand.Parameters[idx].DbType == DbType.AnsiStringFixedLength)
                {
                    sqlCommand.Parameters[idx].Value = CommonLibs.StringLibs.SanitizeString((string)sqlCommand.Parameters[idx].Value);
                }
                switch (sqlCommand.Parameters[idx].DbType)
                {
                    //Null date time
                    case DbType.DateTime:
                    case DbType.Date:
                        if ((DateTime)sqlCommand.Parameters[idx].Value == Consts.NullDate) sqlCommand.Parameters[idx].Value = DBNull.Value;
                        else sqlCommand.Parameters[idx].Value = (DateTime)sqlCommand.Parameters[idx].Value;
                        break;
                    //Null short
                    case DbType.Int16:
                        if ((short)sqlCommand.Parameters[idx].Value == Consts.NullShort) sqlCommand.Parameters[idx].Value = DBNull.Value;
                        else sqlCommand.Parameters[idx].Value = (short)sqlCommand.Parameters[idx].Value;
                        break;
                    //Null int
                    case DbType.Int32:
                        if ((int)sqlCommand.Parameters[idx].Value == Consts.NullInt) sqlCommand.Parameters[idx].Value = DBNull.Value;
                        else sqlCommand.Parameters[idx].Value = (int)sqlCommand.Parameters[idx].Value;
                        break;
                    //Null string
                    case DbType.String:
                        if ((string)sqlCommand.Parameters[idx].Value == Consts.NullString) sqlCommand.Parameters[idx].Value = DBNull.Value;
                        else sqlCommand.Parameters[idx].Value = (string)sqlCommand.Parameters[idx].Value;
                        break;
                }
            }
        }

        /// <summary>
        /// Finallize SQL command before execution
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="addWithReCompile">Forcing the query optimizer to recompile a query plan the next time the same query is executed.</param>
        /// <returns></returns>
        internal static string FinalizeSQL(string sql, bool addWithReCompile = true)
        {
            //return sql + CommonLibs.Consts.constCRLF + "OPTION(ROBUST PLAN)";
            return sql;
            //if (string.IsNullOrWhiteSpace(sql)) return sql;
            //return sql + CommonLibs.Consts.constCRLF + "OPTION(RECOMPILE)";
        }

        internal static bool LoadFromSQL(DataTable tbl, SqlCommand sqlCommand)
        {
            return LoadFromSQL(tbl, sqlCommand, null);
        }
        internal static bool LoadFromSQL(DataTable tbl, SqlCommand sqlCommand, ReliableSqlConnection conn = null)
        {
            if (String.IsNullOrWhiteSpace(sqlCommand.CommandText)) return false;

            ValidateParams(sqlCommand);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            ReliableSqlConnection newConn = (conn != null ? conn : OpenConnection());
            sqlCommand.Connection = newConn.Current;
            sqlCommand.CommandTimeout = newConn.ConnectionTimeout;
            using (var rdr = sqlCommand.ExecuteReader())
            {
                tbl.Load(rdr);
            }
            if (conn == null) CloseConnection(newConn);

            sw.Stop();
            return true;
        }
        internal static ReliableSqlConnection OpenConnection()
        {
            ReliableSqlConnection conn = new ReliableSqlConnection(DataConfig.Configuration.sysDbConnectionStr);
            conn.Open();
            return conn;
        }
        internal static void CloseConnection(ReliableSqlConnection conn)
        {
            if (conn == null) return;
            conn.Close();
            conn.Dispose();
        }

        internal static int ExcuteNonQuerySQL(SqlCommand sqlCommand, ReliableSqlConnection conn = null)
        {
            ValidateParams(sqlCommand);
            ReliableSqlConnection newConn = (conn != null ? conn : OpenConnection());
            Stopwatch sw = new Stopwatch(); sw.Start();

            sqlCommand.Connection = newConn.Current;
            sqlCommand.CommandTimeout = newConn.ConnectionTimeout;
            int count = sqlCommand.ExecuteNonQueryWithRetry();
            if (conn == null) CloseConnection(newConn);

            sw.Stop();
            return count;
        }

        internal static object ExecuteScalarSQL(SqlCommand sqlCommand, ReliableSqlConnection conn = null)
        {
            ValidateParams(sqlCommand);
            ReliableSqlConnection newConn = (conn != null ? conn : OpenConnection());

            Stopwatch sw = new Stopwatch(); sw.Start();
            sqlCommand.Connection = newConn.Current;
            sqlCommand.CommandTimeout = newConn.ConnectionTimeout;
            object obj = sqlCommand.ExecuteScalarWithRetry();
            if (conn == null) CloseConnection(newConn);

            sw.Stop();
            return obj;
        }

        //#endregion
        internal static void Add(StringCollection fldList, string fldName, string alias, QueryType queryType)
        {
            switch (queryType)
            {
                case QueryType.Insert: fldList.Add(String.Format("{0}", fldName)); break;
                case QueryType.Update: fldList.Add(String.Format("{0}=@{0}", fldName)); break;
                case QueryType.Select: fldList.Add(String.Format("{0}.{1}", alias, fldName)); break;
            }
        }

        internal static string GetTableName(TblNames t)
        {
            return EngineLibs.GetTableName(t.ToString());
        }
        internal static string GetTableName(string key)
        {
            if (myTableList.ContainsKey(key)) return myTableList[key].Name;
            return null;
        }
        internal static string GetTableAlias(TblNames t, string aliasPrefix = null)
        {
            return EngineLibs.GetTableAlias(t.ToString(), aliasPrefix);
        }
        internal static string GetTableAlias(string key, string aliasPrefix = null)
        {
            if (myTableList.ContainsKey(key))
            {
                if (String.IsNullOrWhiteSpace(aliasPrefix)) return myTableList[key].Alias;
                return aliasPrefix.Trim() + myTableList[key].Alias;
            }
            return null;
        }

        internal static string GetFieldName(TblNames t, string fldName, bool withAlias = true, string aliasPrefix = null)
        {
            if (withAlias == false) return fldName;

            string alias = alias = EngineLibs.GetTableAlias(t, aliasPrefix);
            if (alias == null) return null;
            return alias + "." + fldName;
        }

        internal static string GetFieldName(TblNames t, FldNames f, bool withAlias = true)
        {
            string alias = "";
            if (withAlias)
            {
                alias = EngineLibs.GetTableAlias(t);
                if (alias == null) return null;
                alias += ".";
            }
            string fldName = myFieldList.Find(t, f);
            if (fldName == null) return null;
            return alias + fldName;
        }

        internal static string GetNewParam(SqlCommand sqlCmd, string varSuffix = null)
        {

            if (String.IsNullOrWhiteSpace(varSuffix)) varSuffix = "var";
            varSuffix = varSuffix.ToLower();

            string varName = "";
            int count = sqlCmd.Parameters.Count;
            while (true)
            {
                bool varFound = false;
                varName = varSuffix + count.ToString();
                for (int idx = 0; idx < sqlCmd.Parameters.Count; idx++)
                {
                    if (sqlCmd.Parameters[idx].ParameterName.ToLower() == varName)
                    {
                        varFound = true;
                        break;
                    }
                }
                if (varFound == false) break;
                count++;
            }
            return varName;
        }
        internal static string AddSqlNewParam(SqlCommand sqlCommand, string varSuffix, object value)
        {
            string varName = GetNewParam(sqlCommand, varSuffix);
            AddSqlParam(sqlCommand, varName, value);
            return varName;
        }
        internal static void AddSqlParam(SqlCommand sqlCommand, string varName, object value, SqlDbType? type = null)
        {
            if (sqlCommand == null) return;
            if (value == null) sqlCommand.Parameters.AddWithValue(varName, DBNull.Value);
            else sqlCommand.Parameters.AddWithValue(varName, value);
            if (type != null) sqlCommand.Parameters[varName].SqlDbType = type.Value;
        }

        internal static string GetJoinTypeCmd(JoinTypes jt)
        {
            switch (jt)
            {
                case JoinTypes.Left: return "LEFT OUTER JOIN";
                case JoinTypes.Right: return "RIGHT OUTER JOIN";
                case JoinTypes.Full: return "FULL OUTER JOIN";
                case JoinTypes.Cross: return "CROSS JOIN";
                default: return "JOIN";
            }
        }

        internal static void BuildFilter(EngineInfo engine, StringCollection list, string fldName, string varName, SearchOptions option)
        {
            if (String.IsNullOrWhiteSpace(fldName)) return;

            string filter = "", tmp = "";
            filter = "";

            if (option == SearchOptions.Exact)
            {
                filter = CommonLibs.SysLibs.MakeConditionStr(list, "N'", "'", ",");
                if (String.IsNullOrEmpty(filter) == false)
                {
                    filter = fldName + " IN(" + filter + ")";
                }
            }
            else
            {
                foreach (string str in list)
                {
                    string newVarName = engine.GetNewParam(varName);
                    tmp = EngineLibs.BuildFilter(engine.SqlCommand, fldName, str, newVarName, option);
                    filter += (filter == "" ? "" : " OR ") + tmp;
                }
            }
            engine.AddFilter(filter);
        }
        internal static void BuildFilter(EngineInfo engine, string fldName, string searchTerm, string varName, SearchOptions option)
        {
            if (String.IsNullOrWhiteSpace(fldName)) return;
            string filter = EngineLibs.BuildFilter(engine.SqlCommand, fldName, searchTerm, varName, option);
            engine.AddFilter(filter);
        }

        internal static string BuildFilter(SqlCommand sqlCmd, string searchBy, string searchTerm, string varName, SearchOptions option)
        {
            string tmpFilter = "";
            switch (option)
            {
                case SearchOptions.Exact:
                    tmpFilter = searchBy + "=@" + varName;
                    AddSqlParam(sqlCmd, varName, searchTerm);
                    break;
                case SearchOptions.EndWith:
                    tmpFilter = searchBy + " LIKE @" + varName;
                    AddSqlParam(sqlCmd, varName, CommonLibs.Consts.SQL_CMD_ALL_MARKER + searchTerm);
                    break;
                case SearchOptions.StartWith:
                    tmpFilter = searchBy + " LIKE @" + varName;
                    AddSqlParam(sqlCmd, varName, searchTerm + CommonLibs.Consts.SQL_CMD_ALL_MARKER);
                    break;
                case SearchOptions.Contain:
                    tmpFilter = searchBy + " LIKE @" + varName;
                    AddSqlParam(sqlCmd, varName, CommonLibs.Consts.SQL_CMD_ALL_MARKER + searchTerm + CommonLibs.Consts.SQL_CMD_ALL_MARKER);
                    break;
                case SearchOptions.HaveAll:
                    tmpFilter = "";
                    string[] items = searchTerm.Split(Settings.sysWordSeparators, StringSplitOptions.RemoveEmptyEntries);
                    for (int idx = 0; idx < items.Length; idx++)
                    {
                        string newVarName = AddSqlNewParam(sqlCmd, varName, CommonLibs.Consts.SQL_CMD_ALL_MARKER + items[idx] + CommonLibs.Consts.SQL_CMD_ALL_MARKER);
                        tmpFilter += (tmpFilter == "" ? "" : " AND ") + searchBy + " LIKE @" + newVarName;
                    }
                    break;

                case SearchOptions.ftContainAnd:
                case SearchOptions.ftContainNear:
                    tmpFilter = ftBuildFilter(searchTerm, (option == SearchOptions.ftContainNear ? "NEAR" : "AND"));
                    if (string.IsNullOrWhiteSpace(tmpFilter) == false)
                    {
                        AddSqlParam(sqlCmd, varName, tmpFilter);
                        tmpFilter = string.Format("CONTAINS({0},@{1})", searchBy, varName);
                    }
                    break;
            }
            return tmpFilter;
        }

        /// <summary>
        /// Return filter for full-text search
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="op"></param>
        /// <returns></returns>
        internal static string ftBuildFilter(string searchTerm, string joinOp = null)
        {
            if (string.IsNullOrWhiteSpace(searchTerm)) return null;

            if (string.IsNullOrWhiteSpace(joinOp)) joinOp = "NEAR";
            joinOp = " " + joinOp.Trim() + " ";

            string filter = "", tmp;
            int quoteStartIdx = -1;
            int processedIdx = -1;

            int quotedTermCount = 0, singleTermCount = 0;
            for (int idx = 0; idx < searchTerm.Length; idx++)
            {
                //String between quote
                if (searchTerm[idx] == '"')
                {
                    if (quoteStartIdx >= 0)
                    {
                        tmp = searchTerm.Substring(quoteStartIdx + 1, idx - quoteStartIdx - 1);
                        tmp = tmp.Trim();
                        if (tmp.Length > 0)
                        {
                            //If having many words, put them in the quotes : find exactly
                            if (tmp.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length > 1)
                            {
                                if (string.IsNullOrWhiteSpace(tmp) == false)
                                {
                                    filter += (filter == "" ? "" : joinOp) + "\"" + tmp + "\"";
                                    quotedTermCount++;
                                }
                            }
                            else
                            {
                                tmp = ftValidateWord(tmp);
                                if (string.IsNullOrWhiteSpace(tmp) == false)
                                {
                                    filter += (filter == "" ? "" : joinOp) + tmp;
                                    singleTermCount++;
                                }
                            }
                        }
                        quoteStartIdx = -1;
                        processedIdx = idx;
                    }
                    else
                    {
                        quoteStartIdx = idx;
                        //Any single word missed ?
                        if (idx > processedIdx)
                        {
                            tmp = ftValidateWord(searchTerm.Substring(processedIdx + 1, idx - processedIdx - 1));
                            if (string.IsNullOrWhiteSpace(tmp) == false)
                            {
                                filter += (filter == "" ? "" : joinOp) + tmp;
                                singleTermCount++;
                            }
                            processedIdx = idx;
                        }
                    }
                    continue;
                }
                //String word outside the quotion mark
                if (quoteStartIdx < 0 && searchTerm[idx] == ' ')
                {
                    if (idx > processedIdx)
                    {
                        tmp = ftValidateWord(searchTerm.Substring(processedIdx + 1, idx - processedIdx - 1));
                        if (string.IsNullOrWhiteSpace(tmp) == false)
                        {
                            filter += (filter == "" ? "" : joinOp) + tmp;
                            singleTermCount++;
                        }
                        processedIdx = idx;
                    }
                    continue;
                }
            }
            //Check if there is some word left
            if (searchTerm.Length > processedIdx)
            {
                tmp = ftValidateWord(searchTerm.Substring(processedIdx + 1, searchTerm.Length - processedIdx - 1));
                if (string.IsNullOrWhiteSpace(tmp) == false) filter += (filter == "" ? "" : joinOp) + tmp;
            }
            //There are some unknown behavior of FTS; we must instruct it to find all words OR the search pharse.
            //tmpFilter = string.Format("({0}) OR '\"{1}\"'", tmpFilter, searchTerm);
            tmp = searchTerm.Replace("\"", " ");
            tmp = CommonTypes.AppLibs.StandardizeString(tmp);
            if (tmp.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length > 1)
            {
                if (string.IsNullOrWhiteSpace(filter) == false) filter = filter + " OR ";
                filter += "\"" + tmp + "\"";
            }
            return filter;
        }

        /// <summary>
        /// Ignore SQL keyword and return valid word
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        internal static string ftValidateWord(string word)
        {
            if (string.IsNullOrWhiteSpace(word)) return null;
            word = CommonTypes.AppLibs.StandardizeString(word).ToLower();
            if (word == "and" || word == "or" || word == "near" || word == "contains") return null;
            word = word.Replace(" ", "");
            word = word.Replace("\"", "");
            if (word.Length > 0 && char.IsLetter(word[0]) == false) word = "\"" + word + "\"";
            return word;
        }

        /// <summary>
        /// Make param var name
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="varName"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        internal static string MakeParamVarName(string prefix, string varName, string suffix = null)
        {
            return string.Format("{0}{1}{2}", prefix, varName, suffix);
        }
    }
}

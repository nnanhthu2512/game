﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using static Services.CloudStorage;
using System.Threading.Tasks;
using System.Globalization;

namespace Services
{
    internal class CloudStorage
    {
        const int PARALLELISM_COUNT = 10;
        const int SPLIT_BLOB_THRESHOLD_IN_BYTES = 1024 * 1024; //1MB, the minimum
        const int RETRY_COUNT = 2;
        const int RETRY_BACKOFF_IN_SECONDS = 2;
        const int CLOCK_SKEW_IN_SECONDS = 1 * 60;

        internal class Adapter
        {
            internal CloudStorageAccount StorangeAccount = null;
            internal CloudBlobClient BlobClient = null;
            internal CloudBlobContainer BlobContainer = null;
            internal bool IsLocked = false;
            internal bool IsPublishCloud = false;

            public Adapter(bool isPublish)
            {
                this.Reset(isPublish);
            }

            internal void Reset(bool isPublish)
            {
                try
                {
                    this.StorangeAccount = CloudStorageAccount.Parse(DataConfig.Configuration.sysStorageConnectionStr);
                    this.BlobClient = this.StorangeAccount.CreateCloudBlobClient();
                    this.BlobClient.DefaultRequestOptions = Libs.GetBlobRequestOptions();
                    if(isPublish == false)
                        this.BlobContainer = this.BlobClient.GetContainerReference(Game.Settings.SysUseCloudStorageContainer);
                    else
                        this.BlobContainer = this.BlobClient.GetContainerReference(Game.Settings.SysUseCloudPublicContainer);
                    this.BlobContainer.CreateIfNotExists();
                    this.IsLocked = false;
                    this.IsPublishCloud = isPublish;

                }
                catch (Exception er)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(er);
                }
            }

            internal void Lock() { this.IsLocked = true; }
            internal void UnLock() { this.IsLocked = false; }
        }

        internal class AdapterList : List<Adapter>
        {
            internal Adapter GetFreeAdapter(bool isPublish)
            {
                int idx = this.FindIndex(x => x.IsLocked == false && isPublish == x.IsPublishCloud);
                if (idx >= 0)
                {
                    this[idx].Lock();
                    return this[idx];
                }
                Adapter ad = new Adapter(isPublish);
                this.Add(ad);
                ad.Lock();
                return ad;
            }
        }


        internal static class Libs
        {
            static CommonTypes.TokenTbl MyStreamTbl = new CommonTypes.TokenTbl();

            internal static void Reset()
            {
                MyAdapterList.Clear();
                MyStreamTbl.Reset();
            }

            //See http://justazure.com/azure-blob-storage-part-4-uploading-large-blobs/
            internal static BlobRequestOptions GetBlobRequestOptions()
            {
                BlobRequestOptions option = new BlobRequestOptions()
                {
                    SingleBlobUploadThresholdInBytes = SPLIT_BLOB_THRESHOLD_IN_BYTES,
                    ParallelOperationThreadCount = PARALLELISM_COUNT,
                    RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(RETRY_BACKOFF_IN_SECONDS), RETRY_COUNT),
                    StoreBlobContentMD5 = true,
                    UseTransactionalMD5 = true,
                };
                return option;
            }

            private static AdapterList MyAdapterList = new AdapterList();

            internal static Adapter GetFreeAdapter(bool isPublish)
            {
                return MyAdapterList.GetFreeAdapter(isPublish);
            }

            /// <summary>
            /// Cloud path MUST NOT start with a slash.
            /// Check to remove it.
            /// </summary>
            /// <param name="filePath"></param>
            internal static bool ValidatePath(ref string filePath)
            {
                if (filePath == null) return false;
                if (filePath.StartsWith(@"\")) filePath = filePath.Substring(1);
                return string.IsNullOrEmpty(filePath) == false;
            }

            /// <summary>
            /// Return root folder that store all data (system, upload...)
            /// Root folder must NOT start with a / and is located under storage's container
            /// </summary>
            /// <param name="folderType"></param>
            /// <returns></returns>
            public static string GetRootFolderPath(CommonTypes.FolderTypes? folderType = null)
            {
                return folderType == null ? "" : folderType.ToString();
            }

            /// <summary>
            /// Return folder path storing system files such as template, usagetip files
            /// </summary>
            /// <param name="type">Category</param>
            /// <param name="lang"></param>
            /// <returns></returns>
            public static string GetSysGameFolderPath(CommonTypes.SystemFileType? type = null, CommonTypes.SystemFolderType? folder = null, CommonTypes.SystemFolderImg? folderImg = null,  CommonTypes.SysLanguage? lang = null)
            {
                string filePath = GetRootFolderPath();//CommonTypes.FolderTypes.System
                if (type != null) filePath = CommonLibs.FileSystem.Combine(filePath, type.ToString(), folder.ToString(), folderImg.ToString());
                if (lang != null) filePath = CommonLibs.FileSystem.Combine(filePath, CommonLibs.SysLibs.GetCultureCode(lang.Value));
                return filePath;
            }


            /// <summary>
            /// Return URI to access a file on cloud.
            /// </summary>
            /// <param name="filePath"></param>
            /// <param name="importance"></param>
            /// <returns></returns>
            internal static string GetFileURI(string filePath, CommonTypes.SignatureImportance importance)
            {
                if (string.IsNullOrWhiteSpace(filePath)) return null;
                filePath = filePath.Replace('\\', '/');

                DateTime dt = new DateTime(2016, 12, 5, 0, 0, 0, 0); //DateTime.UtcNow;
                DateTime startDt = dt.AddSeconds(-CLOCK_SKEW_IN_SECONDS);
                DateTime endDt = CommonLibs.SysLibs.GetExpirationTime(importance, dt);
                if (endDt != DateTime.MaxValue) endDt = endDt.AddSeconds(CLOCK_SKEW_IN_SECONDS);
                bool isCloudPublish = false;
                if (importance == CommonTypes.SignatureImportance.None)
                    isCloudPublish = true;
                return Access.GetAdhocURL(filePath, SharedAccessBlobPermissions.Read, startDt, endDt, isCloudPublish);
            }


        }
    }

    /// <summary>
    /// SAS (Shared access signature)
    /// http://justazure.com/azure-blob-storage-part-9-shared-access-signatures/
    /// https://azure.microsoft.com/en-us/documentation/articles/storage-dotnet-shared-access-signature-part-1/#best-practices-for-using-shared-access-signatures
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="importance"></param>
    /// <returns></returns>
    internal static class Access
    {
        /// <summary>
        /// Get adhoc URL to access a file on cloud.
        /// </summary>
        /// <param name="permissions"></param>
        /// <param name="filePath"></param>
        /// <param name="startDt"></param>
        /// <param name="endDt"></param>
        /// <returns>Null if error</returns>
        internal static string GetAdhocURL(string filePath, SharedAccessBlobPermissions permissions, DateTime startDt, DateTime endDt, bool isCloudPublish = false)
        {
            if (Libs.ValidatePath(ref filePath) == false) return null;
            Adapter adapter = Libs.GetFreeAdapter(isCloudPublish);
            try
            {
                CloudBlockBlob blob = adapter.BlobContainer.GetBlockBlobReference(filePath);
                if (blob == null || blob.Exists() == false) return null;
                if (isCloudPublish == true)
                { return string.Format(CultureInfo.InvariantCulture, "{0}", blob.Uri); }

                SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy();
                policy.Permissions = permissions;
                policy.SharedAccessStartTime = startDt;
                policy.SharedAccessExpiryTime = endDt;

                var sasToken = blob.GetSharedAccessSignature(policy);
                return string.Format(CultureInfo.InvariantCulture, "{0}{1}", blob.Uri, sasToken);
            }
            catch (Exception er)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(er);
                return null;
            }
            finally
            {
                adapter.UnLock();
            }
        }

        ///// <summary>
        ///// Create store access policy to control the file access
        ///// </summary>
        ///// <param name="policyName"></param>
        ///// <param name="blobPermissions"></param>
        ///// <param name="expireDate"></param>
        ///// <returns>Null if error</returns>
        //internal async static Task<Adapter> CreatePolicy(string policyName, SharedAccessBlobPermissions blobPermissions, DateTime expireDate)
        //{
        //    Adapter adapter = Libs.GetFreeAdapter();
        //    try
        //    {
        //        SharedAccessBlobPolicy storedPolicy = new SharedAccessBlobPolicy()
        //        {
        //            SharedAccessExpiryTime = expireDate,
        //            Permissions = blobPermissions
        //        };

        //        //let’s start with a new collection of permissions (this wipes out any old ones)
        //        BlobContainerPermissions permissions = new BlobContainerPermissions();

        //        //add the new policy to the container's permissions
        //        //since this is the only one I want, I'm going to clear the rest first
        //        permissions.SharedAccessPolicies.Clear();
        //        permissions.SharedAccessPolicies.Add(policyName, storedPolicy);

        //        await adapter.BlobContainer.SetPermissionsAsync(permissions);
        //        return adapter;
        //    }
        //    catch (Exception er)
        //    {
        //        CommonLibs.ErrorLibs.WriteErrorLog(er);
        //        return null;
        //    }
        //    finally
        //    {
        //        adapter.UnLock();
        //    }
        //}

        ///// <summary>
        ///// Get URL to access a file on cloud governed by store access policy.
        ///// </summary>
        ///// <param name="filePath"></param>
        ///// <param name="policyName"></param>
        ///// <param name="bobPermissions"></param>
        ///// <param name="expireDate"></param>
        ///// <returns></returns>
        //private async static Task<string> GetAccessURL(string filePath, string policyName, SharedAccessBlobPermissions bobPermissions, DateTime expireDate)
        //{
        //    Adapter adapter = await CreatePolicy(policyName, bobPermissions, expireDate);
        //    CloudBlockBlob cloudBlockBlob = adapter.BlobContainer.GetBlockBlobReference(filePath);

        //    //using that shared access policy, get the sas token and set the url
        //    string sasToken = cloudBlockBlob.GetSharedAccessSignature(null, policyName);
        //    return string.Format(CultureInfo.InvariantCulture, "{0}{1}", cloudBlockBlob.Uri, sasToken);
        //}

        /// <summary>
        /// Return store access policy name for user to access file path
        /// </summary>
        /// <param name="byUserId"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        internal static string MakePolicyName(int byUserId, string filePath)
        {
            string policyName = filePath.Replace('\\', '_').Replace('/', '_');
            return string.Format("sas{0}-{1}", byUserId, policyName);
        }

        ///// <summary>
        ///// Return URL to ReadOnly access to a file
        ///// </summary>
        ///// <param name="byUserId"></param>
        ///// <param name="filePath"></param>
        ///// <param name="expireDate"></param>
        ///// <returns></returns>
        //private async static Task<string> GetUrlRO(int byUserId, string filePath, DateTime expireDate)
        //{
        //    string policyName = MakePolicyName(byUserId, filePath);
        //    return await GetAccessURL(filePath, policyName, SharedAccessBlobPermissions.Read, expireDate);
        //}

        ///// <summary>
        ///// Remove access policy :  access to file governed by the policy is eliminated
        ///// </summary>
        ///// <param name="policyName"></param>
        ///// <returns></returns>
        //internal async static Task<bool> RemoveAccessPolicy(string policyName)
        //{
        //    Adapter adapter = Libs.GetFreeAdapter();
        //    try
        //    {
        //        var containerPermissions = await adapter.BlobContainer.GetPermissionsAsync();
        //        containerPermissions.SharedAccessPolicies.Remove(policyName);
        //        await adapter.BlobContainer.SetPermissionsAsync(containerPermissions);
        //        return true;
        //    }
        //    catch (Exception er)
        //    {
        //        CommonLibs.ErrorLibs.WriteErrorLog(er);
        //        return false;
        //    }
        //    finally
        //    {
        //        adapter.UnLock();
        //    }
        //}

        ///// <summary>
        ///// Remove access by user to file
        ///// </summary>
        ///// <param name="byUserId"></param>
        ///// <param name="filePath"></param>
        ///// <returns></returns>
        //internal async static Task<bool> RemoveAccess(int byUserId, string filePath)
        //{
        //    return await RemoveAccessPolicy(MakePolicyName(byUserId, filePath));
        //}
    }

}

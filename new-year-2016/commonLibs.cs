﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataEngine.cs 								                    */
/* Description : Common classes and functions supporting to retrieve/search     */
/* module.                                                                      */
/*													                            */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using System;
using System.Xml;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Globalization;
using CommonTypes;
using Game;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace CommonLibs
{
    #region Use in CommonLibs
    internal static class cryption
    {
        static cryption()
        {
            SetEncryptionKey("sBdsddw43vx,vAfdfdlSDZ3sUS2s4+-|16)(*");
        }

        public static void SetEncryptionKey(string key)
        {
            myEncryptionKey = MakeEncryptionKey(key, true); ;
        }

        private static byte[] myEncryptionKey = null;

        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="str">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        public static string Decrypt(string str)
        {

            AesCryptoServiceProvider myCryptProvider = MakeEncryptProvider();
            byte[] keyArray = myEncryptionKey;
            byte[] toEncryptArray = Convert.FromBase64String(str);

            ICryptoTransform cTransform = myCryptProvider.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        private static AesCryptoServiceProvider MakeEncryptProvider()
        {
            AesCryptoServiceProvider cryptProvider = new AesCryptoServiceProvider();
            cryptProvider.Key = myEncryptionKey;
            cryptProvider.Mode = CipherMode.CBC;
            cryptProvider.Padding = PaddingMode.ISO10126;
            cryptProvider.BlockSize = 128;
            cryptProvider.IV = new byte[16] { 40, 51, 12, 25, 36, 112, 123, 210, 126, 127, 128, 129, 111, 124, 215, 246 };
            return cryptProvider;
        }

        /// <summary>
        /// Protect from code reverse
        /// </summary>
        /// <param name="key"></param>
        /// <param name="useHashing"></param>
        /// <returns></returns>
        private static byte[] MakeEncryptionKey(string key, bool useHashing)
        {
            const int hashChunkSz = 16;
            if (useHashing)
            {
                string subKey = "";
                int sz = 0;
                while (sz < key.Length)
                {
                    if (sz + hashChunkSz > key.Length)
                        subKey += Hash.MakeHash(key.Substring(sz, key.Length - sz), CommonTypes.HashType.MD5, hashChunkSz);
                    else subKey += Hash.MakeHash(key.Substring(sz, hashChunkSz), CommonTypes.HashType.MD5, hashChunkSz);
                    sz += hashChunkSz;
                }
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                byte[] keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
                return keyArray;
            }
            return UTF8Encoding.UTF8.GetBytes(key);
        }

        public static string Encrypt(string str)
        {
            AesCryptoServiceProvider myCryptProvider = MakeEncryptProvider();
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(str);

            ICryptoTransform cTransform = myCryptProvider.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            myCryptProvider.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string SimpleCrypt(string data, bool useHashing = true)
        {
            byte[] key = myEncryptionKey;
            string retValue = "";

            int i = 0, x = 0;

            int[] cipher = new int[data.Length];
            x = 0;

            for (i = 0; i < data.Length; i++)
            {
                cipher[i] = (data[i] ^ key[x]);
                retValue = retValue + (char)cipher[i];
                x++;

                if (x >= key.Length) x = 0;
            }
            return retValue;
        }

    }

    public static class Hash
    {
        public static string MakeHash(string input, HashType hashtype, int? maxSize = null)
        {
            input = input.Replace(" ", "");

            Byte[] clearBytes;
            Byte[] hashedBytes;
            string output = String.Empty;

            switch (hashtype)
            {
                case HashType.RIPEMD160:
                    clearBytes = new UTF8Encoding().GetBytes(input);
                    RIPEMD160 myRIPEMD160 = RIPEMD160Managed.Create();
                    hashedBytes = myRIPEMD160.ComputeHash(clearBytes);
                    output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                    break;
                case HashType.MD5:
                    clearBytes = new UTF8Encoding().GetBytes(input);
                    hashedBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(clearBytes);
                    output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                    break;
                case HashType.SHA1:
                    clearBytes = Encoding.UTF8.GetBytes(input);
                    SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
                    sha1.ComputeHash(clearBytes);
                    hashedBytes = sha1.Hash;
                    sha1.Clear();
                    output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                    break;
                case HashType.SHA256:
                    clearBytes = Encoding.UTF8.GetBytes(input);
                    SHA256 sha256 = new SHA256Managed();
                    sha256.ComputeHash(clearBytes);
                    hashedBytes = sha256.Hash;
                    sha256.Clear();
                    output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                    break;
                case HashType.SHA384:
                    clearBytes = Encoding.UTF8.GetBytes(input);
                    SHA384 sha384 = new SHA384Managed();
                    sha384.ComputeHash(clearBytes);
                    hashedBytes = sha384.Hash;
                    sha384.Clear();
                    output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                    break;
                case HashType.SHA512:
                    clearBytes = Encoding.UTF8.GetBytes(input);
                    SHA512 sha512 = new SHA512Managed();
                    sha512.ComputeHash(clearBytes);
                    hashedBytes = sha512.Hash;
                    sha512.Clear();
                    output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                    break;
            }
            if (maxSize != null && output.Length > maxSize) output = output = output.Substring(0, maxSize.Value);
            return output;
        }

        public static string MakeHash(string input, int? maxSize = null)
        {
            string tmp = MakeHash(input, HashType.SHA256);
            if (maxSize != null && tmp.Length > maxSize) return tmp.Substring(1, maxSize.Value);
            return tmp;
        }
    }

    //public enum HashType
    //{
    //    SHA1 = 0,
    //    SHA256 = 1,
    //    SHA384 = 2,
    //    SHA512 = 3,
    //    MD5 = 4,
    //    RIPEMD160 = 5
    //}

    internal static class SysLog
    {
        internal static string MakeLogString(Exception er, string separator, bool fullInfo)
        {
            if (fullInfo)
                return er.TargetSite.ToString() + separator + er.Source + separator + er.Message.Trim() + separator + er.StackTrace.Trim();
            return er.TargetSite.ToString() + separator + er.Source + separator + er.Message.Trim();
        }

        internal static void WriteLog(string text, string logFileName)
        {
            try
            {
                lock (logFileName)
                {
                    System.IO.StreamWriter logFileStream = new System.IO.StreamWriter(logFileName, true);
                    logFileStream.Write(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") +
                                        Game.Consts.constTab + (text == null ? Game.Consts.constNotAvailable : text) +
                                        Game.Consts.constCRLF);
                    logFileStream.Close();
                }
            }
            catch { }
        }
    }
    #endregion

    public class xmlLibs
    {
        public static XmlDocument OpenXML(string fileName)
        {
            //if (!FileLibs.FileExist(fileName)) CreateEmptyXML(fileName);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            return xmlDoc;
        }

        public static string MakeXmlPath(string[] nodeNames)
        {
            return StringLibs.MakeString("/", nodeNames);
        }

        public static string GetElement(XmlDocument xmlDoc, string xmlPath, string nodeName, bool withEncryption)
        {
            XmlNode node = xmlDoc.SelectSingleNode(xmlPath);
            if (node == null) return null;
            string value = null;
            if (StringLibs.IsNullOrWhiteSpace(nodeName)) value = node.InnerText;
            else
            {
                node = FindElement(node, nodeName);
                if (node == null) return null;
                value = node.InnerText;
            }
            if (withEncryption && StringLibs.IsNullOrWhiteSpace(value) == false)
                return cryption.Decrypt(value);
            return value;
        }

        private static XmlElement FindElement(XmlNode node, string nodeName)
        {
            try
            {
                return node[nodeName];
            }
            catch { return null; }
        }
    }

    public static class StringLibs
    {
        public static bool IsNullOrWhiteSpace(string str)
        {
            if (str == null) return true;
            return (str.Trim() == "");
        }

        public static string MakeString(string connector, params string[] list)
        {
            string retStr = "";
            foreach (string arg in list)
            {
                if (IsNullOrWhiteSpace(arg)) continue;
                string str = arg.Trim();
                retStr += (retStr == "" || retStr.EndsWith(connector) ? "" : connector);
                retStr += (str.StartsWith(connector) ? str.Substring(connector.Length) : str);
            }
            return retStr;
        }

        /// <summary>
        /// Sanitize by removing SQL quote in string 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SanitizeString(string str)
        {
            if (str == null) return str;
            return str.Replace("'", "");
        }

        public static string ToString(StringCollection list, string separator, string prefix, string postfix)
        {
            if (list == null || list.Count == 0) return "";
            string retStr = "";
            foreach (string str in list)
            {
                if (str == null || str.Trim() == "") continue;
                retStr += (retStr == "" ? "" : separator) + prefix + str + postfix;
            }
            return retStr;
        }

        /// <summary>
        /// Convert accented chars into plain char
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ReplaceAccentedText(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

    }

    public static class ErrorLibs
    {
        public static void ShowErrorMessage(string msg)
        {
            MessageBox.Show(msg, Settings.SysApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static void WriteErrorLog(Exception er)
        {

            WriteLog(SysLog.MakeLogString(er, Game.Consts.constCRLF, true), Game.Consts.FileErrorLog);
        }

        public static void WriteErrorLog(string text)
        {
            WriteLog(text, Game.Consts.FileErrorLog);
        }

        public static void WriteLog(string text, string toFile = Game.Consts.FileAppLog)
        {
            try
            {
                string filePath = FileSystem.GetRootFolderPath();
                FileSystem.CreateFolder(filePath);
                filePath = FileSystem.Combine(filePath, toFile);
                SysLog.WriteLog(text, filePath);
            }
            catch { }
        }

        public static void WriteDebugLog(string text)
        {
            try
            {
                WriteLog(text, Game.Consts.FileDebugLog);
            }
            catch { }
        }
    }

    public static class SysLibs
    {
        /// <summary>
        /// Get varribale name. For example : GetName(new {varriable})
        /// See : http://abdullin.com/journal/2008/12/13/how-to-find-out-variable-or-parameter-name-in-c.html
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetName<T>(T item) where T : class
        {
            var properties = typeof(T).GetProperties();
            return properties[0].Name;
        }

        #region culture
        private static Dictionary<CommonTypes.SysLanguage, LanguageInfo> myLanguageList = new Dictionary<CommonTypes.SysLanguage, LanguageInfo>();
        public static LanguageInfo GetLanguageInfo(SysLanguage lang)
        {
            lock (myLanguageList)
            {
                if (myLanguageList.ContainsKey(lang))
                {
                    return myLanguageList[lang];
                }
                LanguageInfo info = new LanguageInfo(lang);
                myLanguageList.Add(lang, info);
                return info;
            }
        }
        public static CultureInfo GetCulture(SysLanguage lang)
        {
            return GetLanguageInfo(lang).Culture;
        }

        /// <summary>
        /// Get language from culture code 
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <returns></returns>
        public static SysLanguage GetLanguage(string cultureCode)
        {
            CultureInfo culture1 = new CultureInfo(cultureCode);
            if (culture1 == null) return Game.Consts.DefaultLanguage;

            foreach (SysLanguage lang in Enum.GetValues(typeof(SysLanguage)))
            {
                CultureInfo culture2 = GetLanguageInfo(lang).Culture;
                if (culture2 == null) continue;
                if (culture1.Name == culture2.Name) return lang;
            }
            return Game.Consts.DefaultLanguage;
        }

        public static string GetCultureCode(SysLanguage lang)
        {
            return GetLanguageInfo(lang).Culture.Name;
        }
        #endregion

        public static DateTime GetExpirationTime(SignatureImportance type, DateTime? dt = null)
        {
            DateTime expiredDate = (dt == null ? DateTime.UtcNow : dt.Value);
            switch (type)
            {
                case SignatureImportance.Low: return expiredDate.AddMilliseconds(Settings.sysExpiredInMSecSIGNATURE_LOW);
                case SignatureImportance.Average: return expiredDate.AddMilliseconds(Settings.sysExpiredInMSecSIGNATURE_AVERAGE);
                case SignatureImportance.High: return expiredDate.AddMilliseconds(Settings.sysExpiredInMSecSIGNATURE_HIGH);
                case SignatureImportance.Forever: return DateTime.MaxValue;
            }
            return expiredDate;
        }

        /// <summary>
        /// Get Cookie name to identify logon user, see authentication mode in web.config 
        /// </summary>
        /// <returns></returns>
        public static string GetAuthCookieName()
        {
            return System.Web.Security.FormsAuthentication.FormsCookieName;
        }

        internal static string MakeCacheKey(string prefix, params string[] keys)
        {
            string cacheKey = prefix;
            foreach (string str in keys) cacheKey += (string.IsNullOrWhiteSpace(str) ? "" : ".") + str;
            return cacheKey;
        }

        public static string MakeConditionStr(StringCollection items, string prefixStr, string postfixStr, string operatorStr)
        {
            return StringLibs.ToString(items, operatorStr, prefixStr, postfixStr);
        }

    }

    public class Consts
    {
        public const string SQL_CMD_ALL_MARKER = "%";
    }
}

﻿using CommonTypes;
using System;
using System.Collections.Specialized;
using System.Globalization;

namespace Game
{
    public static class Settings
    {
        internal static StringCollection sysSmsUsaNumbers = new StringCollection();
        static Settings()
        {
            try
            {
                sysSmsUsaNumbers.Clear();
                sysSmsUsaNumbers.Add("13017502529"); //Setup in Nexmo website

                CommonTypes.Config.Load();

                if (sysMemcacheServerAddr == null || sysMemcacheServerAddr.Count == 0)
                {
                    CommonLibs.ErrorLibs.WriteDebugLog("sysMemcacheServerAddr: " + sysMemcacheServerAddr[0]);
                    sysMemcacheServerAddr = new StringCollection();
                    sysMemcacheServerAddr.Add("192.168.1.10");//("192.168.1.10");
                }
            }
            catch (Exception er)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(er);
            }
        }

        static CultureInfo _sysCulture = new CultureInfo(Consts.SystemCultureCode);
        public static CultureInfo SysCulture
        {
            get
            {
                return _sysCulture;
            }
        }

        internal static int MinTotalGifts = 15;
        internal static int MaxTotalGifts = 25;

        internal static int WidthGiftSceneDefault = 1170;
        internal static int HeightGiftSceneDefault = 650;
        internal static int MinGiftSize = 10;

        internal static int totalPoints = 200;

        internal static bool SysUseCloudStorage = false;
        internal static bool sysDebugMode = false;
        internal static bool sysUseStrongEncryption = true;


        //Config for data's name file (.xml)
        internal static string SceneFileName = "scene.xml";
        internal static string GiftFileName = "gift.xml";
        internal static string SpotFileName = "spot.xml";

        internal static string SysApplicationName = "Globedr Game";
        internal static string SysUseCloudStorageContainer = "test";
        internal static string SysUseCloudPublicContainer = "pub";

        internal static string GdrApiURL = "http://test-api.globedr.com/api/";

        // Retry strategy : Retry n times, waiting m seconds before the first retry, 
        // then increasing the waiting time by seconds for each retry.
        internal static byte sysRetryStrategyCount = 5;
        internal static byte sysRetryStrategyFirstWaitInSecs = 3;     //Waiting time for the first retry
        internal static byte sysRetryStrategyIncreaseWaitInSecs = 5;  //Increased waiting time for the second, third... retry

        public static int sysExpiredInMSecSIGNATURE_HIGH = 10 * 1000;               //10 second 
        public static int sysExpiredInMSecSIGNATURE_AVERAGE = 60 * 60 * 1000;           //1 hour 
        public static int sysExpiredInMSecSIGNATURE_LOW = 24 * 60 * 60 * 1000;      //1 day

        internal static string sysExcludeSpecialChars = "%*#&?";
        public static string[] sysWordSeparators = new string[] { " ", "-", ",", ";", "." };

        // Config for memCached
        internal static StringCollection sysMemcacheServerAddr = new StringCollection();
        internal static int sysMemcacheServerPort = 11211;
        internal static int sysMemcacheTimeInSecs = 60 * 60;
        internal static bool sysMemcacheEnabled = true;
        public static int sysCacheKeepTrackDataInSecs = 24 * 60 * 60;


        private static string _sysUserConfigFile = null;
        internal static string sysUserConfigFile
        {
            get
            {
                if (_sysUserConfigFile == null)
                {
                    _sysUserConfigFile = CommonLibs.FileSystem.GetFullPath("user.xml");
                }
                return _sysUserConfigFile;
            }
            set
            {
                _sysUserConfigFile = value;
            }
        }

        ///GEO URL
        public static string sysGeoLongLat2AddressURL = "https://maps.google.com/maps/api/geocode/json?latlng={0},{1}&sensor=false&key=AIzaSyDhjwd07QnYDWyO4LISN7aT1c_6s2hNt4w";
    }
}

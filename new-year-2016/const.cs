﻿using CommonTypes;
using System;
using System.Collections.Specialized;

namespace Game
{
    public static class Consts
    {
        //Common key char
        public static string constCRLF = Environment.NewLine; //  "\r\n";

        //Common key char
        public const char constTab = (char)9;

        public const string constNotAvailable = "N/A";

        public static readonly DateTime NullDate = DateTime.MinValue;
        public static short NullShort = short.MinValue;
        public static int NullInt = Int32.MinValue;
        public static string NullString = " ";

        internal const string constXmlRootElement = "Configuration";
        public const SysLanguage DefaultLanguage = SysLanguage.English;


        internal const string FileErrorLog = @"errorGame.txt";
        internal const string FileDebugLog = @"debugGame.txt";
        internal const string FileAppLog = @"applicationGame.log.txt";
        internal const string SystemConf = "globedrGame.conf.xml";
        internal const string ApplicationConf = "globedrGame.conf.app.xml";

        /// <summary>
        /// Folder for application data (Under web root folder)
        /// </summary>
        internal const string FolderApplication = "~/App_Data";
        public const string ProductCode = "GDR";

        internal const string DefaultPhoneFormat = "####-####";

        //Validate Infor
        public const byte MaxLengthFirstName = 20;
        public const byte MaxLengthLastName = 30;
        public const byte MaxLengthEmail = 100;
        public const byte MinLengthPhoneNo = 8;
        public const byte MaxLengthPhoneNo = 15;

        

        public enum NoScene : byte
        {
            Welcome = 1, Rules = 2, Play = 3, Submit = 4
        };

        public const string SystemCultureCode = "en-US";
        internal const string DefaultCountry = "VN";
    }

}

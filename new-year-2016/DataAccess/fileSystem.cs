﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : da.fileStream.cs 		    				                    */
/* Description : Classes and functions to save/get files in web server's folder */
/* or file service. Files are stored in folders which names are hashed          */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/
using CommonTypes;
using Game;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Windows.Forms;

namespace CommonLibs
{
    public static class FileSystem
    {
        /// <summary>
        /// Return root folder for all files
        /// </summary>
        /// <returns></returns>
        internal static string GetRootFolderPath(FolderTypes? type = null)
        {
            string filePath = "";
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(Game.Consts.FolderApplication);
            }
            else
            {
                if (LibsSettings.sysIsDesignMode) filePath = FileLibs.GetTempPath();
                else filePath = EnvironmentLibs.GetExecutePath();
            }
            if (type != null) return FileSystem.Combine(filePath, type.ToString());
            return filePath;
        }

        public static string GetFile(string fileName, SignatureImportance signImportance, SystemFolderType? folder = null, SystemFolderImg? folderImg = null, SysLanguage? lang = null)
        {
            if (Settings.SysUseCloudStorage)
            {
                DataAccess.FileService.CloudFileSystem cloud = new DataAccess.FileService.CloudFileSystem();
                string tempName = FileSystem.Combine(Services.CloudStorage.Libs.GetSysGameFolderPath(SystemFileType.Game, folder, folderImg, lang), fileName);
                return cloud.GetURI(tempName, signImportance);
            }
            return FileSystem.Combine(GetSystemFolderPath(SystemFileType.Game, folder, folderImg, lang), fileName);
        }

        internal static void CreateFolder(string dirName)
        {
            Directory.CreateDirectory(dirName);
            return;
        }

        internal static string Combine(params string[] fileNames)
        {
            string retStr = "";
            foreach (string arg in fileNames)
            {
                if (String.IsNullOrWhiteSpace(arg)) continue;
                retStr = System.IO.Path.Combine(retStr, arg);
            }
            return retStr;
        }

        internal static string GetFullPath(string fileName)
        {
            if (StringLibs.IsNullOrWhiteSpace(fileName)) return "";
            string filePath = FileLibs.FileNameOnly(fileName) + FileLibs.FileExtension(fileName);
            filePath = fileName.Remove(fileName.Length - filePath.Length);
            filePath = Combine(new string[] { filePath, FileLibs.FileNameOnly(fileName) + FileLibs.FileExtension(fileName) });
            return Combine(EnvironmentLibs.GetExecutePath(), filePath);
        }

        /// <summary>
        /// Get full folder path to store system data files
        /// </summary>
        /// <param name="type">Category</param>
        /// <param name="lang"></param>
        /// <returns></returns>
        private static string GetSystemFolderPath(SystemFileType? type = null, SystemFolderType? folder = null, SystemFolderImg? folderImg = null,  SysLanguage? lang = null)
        {
            string filePath = FileSystem.Combine(GetRootFolderPath(), FolderTypes.System.ToString());
            if (type != null) filePath = FileSystem.Combine(filePath, type.ToString(), folder.ToString(), folderImg.ToString());
            if (lang != null) filePath = FileSystem.Combine(filePath, SysLibs.GetCultureCode(lang.Value));
            return filePath;
        }
    }

    internal static class FileLibs
    {
        public static string GetTempPath()
        {
            return System.IO.Path.GetTempPath();
        }

        public static string FileNamePath(string fileName)
        {
            return System.IO.Path.GetDirectoryName(fileName);
        }

        public static string FileNameOnly(string fileName)
        {
            return System.IO.Path.GetFileNameWithoutExtension(fileName);
        }

        public static string FileExtension(string fileName)
        {
            return System.IO.Path.GetExtension(fileName);
        }
    }

    internal static class LibsSettings
    {
        private static int _sysIsDesignModeCode = -1;
        public static bool sysIsDesignMode
        {
            get
            {
                if (_sysIsDesignModeCode < 0)
                {
                    switch (System.Diagnostics.Process.GetCurrentProcess().ProcessName)
                    {
                        case "TE.ProcessHost.Managed":
                        case "vstest.executionengine.x86":
                            _sysIsDesignModeCode = 1; break;
                        default: _sysIsDesignModeCode = 0; break;
                    }
                }
                return (_sysIsDesignModeCode == 1);
            }
        }
    }

    internal static class EnvironmentLibs
    {
        public static string GetExecutePath()
        {
            return FileLibs.FileNamePath(Application.ExecutablePath);
        }
    }
}

namespace DataAccess
{
    internal class FileService
    {
        /// <summary>
        /// File system storing on cloud
        /// </summary>
        internal class CloudFileSystem : CommonModels.FileSystem
        {
            internal string GetURI(string path, SignatureImportance importance = SignatureImportance.Average)
            {
                path = this.MakeFullPath(path);
                string result = Services.CloudStorage.Libs.GetFileURI(path, importance);
                CommonLibs.ErrorLibs.WriteLog(String.Format("Read file {0}", result), "testUploadFile.txt");
                return result;
            }
        }

    }

}

namespace CommonModels
{
    /// <summary>
    /// Represent a system providing method to access/retrieve files
    /// </summary>
    internal class FileSystem
    {
        string RootFolderPath = null;

        internal string MakeFullPath(string path)
        {
            return CombinePath(this.RootFolderPath, path);
        }
        internal string CombinePath(params string[] path)
        {
            string retStr = "";
            foreach (string arg in path)
            {
                if (String.IsNullOrWhiteSpace(arg)) continue;
                retStr = Path.Combine(retStr, arg);
            }
            return retStr;
        }

    }
}
﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataEngine.cs 								                    */
/* Description : Common classes and functions supporting to retrieve/search     */
/* module.                                                                      */
/*													                            */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System.Diagnostics;
using System.Collections.Generic;
using Game;

namespace DataAccess
{
    //public static class EngineLibs
    //{
    //    static EngineLibs()
    //    {
    //        UseRetryPolicyIncremental("defaultRetryStrategy");
    //    }

    //    public static void UseRetryPolicyIncremental(string strategyName)
    //    {
    //        Incremental myStrategy = new Incremental(strategyName, Settings.sysRetryStrategyCount,
    //                                                TimeSpan.FromSeconds(Settings.sysRetryStrategyFirstWaitInSecs),
    //                                                TimeSpan.FromSeconds(Settings.sysRetryStrategyIncreaseWaitInSecs));

    //        RetryPolicy policy = new RetryPolicy<SqlDatabaseTransientErrorDetectionStrategy>(myStrategy);
    //        policy.Retrying += (obj, eventArgs) =>
    //        {
    //            var msg = String.Format("Retrying, CurrentRetryCount = {0} , Delay = {1}, Exception = {2}", eventArgs.CurrentRetryCount, eventArgs.Delay, eventArgs.LastException.Message);
    //            CommonLibs.ErrorLibs.WriteErrorLog(msg);
    //        };
    //        List<RetryStrategy> myStrategies = new List<RetryStrategy> { myStrategy };
    //        RetryManager myManager = new RetryManager(myStrategies, strategyName);
    //        RetryManager.SetDefault(myManager, false);
    //    }
    //    internal static void AddSqlParam(SqlCommand sqlCommand, string varName, object value, SqlDbType? type = null)
    //    {
    //        if (sqlCommand == null) return;
    //        if (value == null) sqlCommand.Parameters.AddWithValue(varName, DBNull.Value);
    //        else sqlCommand.Parameters.AddWithValue(varName, value);
    //        if (type != null) sqlCommand.Parameters[varName].SqlDbType = type.Value;
    //    }

    //internal static int ExcuteNonQuerySQL(SqlCommand sqlCommand, ReliableSqlConnection conn = null)
    //{
    //    ValidateParams(sqlCommand);
    //    ReliableSqlConnection newConn = (conn != null ? conn : OpenConnection());
    //    Stopwatch sw = new Stopwatch(); sw.Start();

    //    sqlCommand.Connection = newConn.Current;
    //    sqlCommand.CommandTimeout = newConn.ConnectionTimeout;
    //    int count = sqlCommand.ExecuteNonQueryWithRetry();
    //    if (conn == null) CloseConnection(newConn);

    //    sw.Stop();
    //    return count;
    //}

    //internal static ReliableSqlConnection OpenConnection()
    //{
    //    ReliableSqlConnection conn = new ReliableSqlConnection(DataConfig.Configuration.sysDbConnectionStr);
    //    conn.Open();
    //    return conn;
    //}
    //    internal static void CloseConnection(ReliableSqlConnection conn)
    //    {
    //        if (conn == null) return;
    //        conn.Close();
    //        conn.Dispose();
    //    }
    //    internal static void ValidateParams(SqlCommand sqlCommand)
    //    {
    //        for (int idx = 0; idx < sqlCommand.Parameters.Count; idx++)
    //        {
    //            if (sqlCommand.Parameters[idx] == null || sqlCommand.Parameters[idx].Value == null || sqlCommand.Parameters[idx].Value == DBNull.Value) continue;
    //            //Prevent SQL injection
    //            if (sqlCommand.Parameters[idx].DbType == DbType.String || sqlCommand.Parameters[idx].DbType == DbType.StringFixedLength ||
    //                sqlCommand.Parameters[idx].DbType == DbType.AnsiString || sqlCommand.Parameters[idx].DbType == DbType.AnsiStringFixedLength)
    //            {
    //                sqlCommand.Parameters[idx].Value = CommonLibs.StringLibs.SanitizeString((string)sqlCommand.Parameters[idx].Value);
    //            }
    //            switch (sqlCommand.Parameters[idx].DbType)
    //            {
    //                //Null date time
    //                case DbType.DateTime:
    //                case DbType.Date:
    //                    if ((DateTime)sqlCommand.Parameters[idx].Value == Consts.NullDate) sqlCommand.Parameters[idx].Value = DBNull.Value;
    //                    else sqlCommand.Parameters[idx].Value = (DateTime)sqlCommand.Parameters[idx].Value;
    //                    break;
    //                //Null short
    //                case DbType.Int16:
    //                    if ((short)sqlCommand.Parameters[idx].Value == Consts.NullShort) sqlCommand.Parameters[idx].Value = DBNull.Value;
    //                    else sqlCommand.Parameters[idx].Value = (short)sqlCommand.Parameters[idx].Value;
    //                    break;
    //                //Null int
    //                case DbType.Int32:
    //                    if ((int)sqlCommand.Parameters[idx].Value == Consts.NullInt) sqlCommand.Parameters[idx].Value = DBNull.Value;
    //                    else sqlCommand.Parameters[idx].Value = (int)sqlCommand.Parameters[idx].Value;
    //                    break;
    //                //Null string
    //                case DbType.String:
    //                    if ((string)sqlCommand.Parameters[idx].Value == Consts.NullString) sqlCommand.Parameters[idx].Value = DBNull.Value;
    //                    else sqlCommand.Parameters[idx].Value = (string)sqlCommand.Parameters[idx].Value;
    //                    break;
    //            }
    //        }
    //    }
    //}

    public static class DbCache
    {
        #region template dataset
        internal static game.myGame myGameDS = new game.myGame();
        #endregion

        #region link-cache tables
        static game.myGame.countryDataTable _countryTbl = null;
        internal static game.myGame.countryDataTable CountryTbl
        {
            get
            {
                if (_countryTbl != null) return _countryTbl;
                CommonTypes.ApiStatusCode exitCode;
                lock (myGameDS.country)
                {
                    _countryTbl = (game.myGame.countryDataTable)Common.GetAndCacheData(out exitCode,
                                                                                         EngineLibs.GetTableAlias(TblNames.Country),
                                                                                         Common.CreateCountryTbl, Common.LoadCountry);
                    return _countryTbl;
                }
            }
        }

        static game.myGame.sysCountrySettingsDataTable _countrySettingsTbl = null;
        internal static game.myGame.sysCountrySettingsDataTable CountrySettings
        {
            get
            {
                if (_countrySettingsTbl != null) return _countrySettingsTbl;
                CommonTypes.ApiStatusCode exitCode;
                lock (myGameDS.sysCountrySettings)
                {
                    _countrySettingsTbl = (game.myGame.sysCountrySettingsDataTable)Common.GetAndCacheData(out exitCode,
                                                                                                            EngineLibs.GetTableAlias(TblNames.SysCountrySettings),
                                                                                                            Common.CreateCountrySettingsTbl, Common.LoadCountrySettings);
                    return _countrySettingsTbl;
                }
            }
        }
        #endregion
    }

    internal static class Common
    {
        const int constCacheTimeInSecs = 24 * 60 * 60;
        const string constCachePrefix = "db";

        /// <summary>
        /// Callback function to create new cached table
        /// </summary>
        /// <returns></returns>
        internal delegate DataTable CreateTableHandler();
        internal delegate CommonTypes.ApiStatusCode LoadDataHandler(DataTable tbl);
        const CommonTypes.CacheType constCacheType = CommonTypes.CacheType.MemCache;

        internal static game.myGame.countryDataTable CreateCountryTbl() { return new game.myGame.countryDataTable(); }
        internal static game.myGame.sysCountrySettingsDataTable CreateCountrySettingsTbl() { return new game.myGame.sysCountrySettingsDataTable(); }


        internal static CommonTypes.ApiStatusCode LoadCountry(DataTable tbl)
        {
            return Load(tbl, string.Format("{0},{1}", DbCache.myGameDS.country.languageColumn.ColumnName,
                                                        DbCache.myGameDS.country.descriptionColumn.ColumnName));
        }
        internal static CommonTypes.ApiStatusCode LoadCountrySettings(DataTable tbl)
        {
            return Load(tbl, string.Format("{0}", DbCache.myGameDS.sysCountrySettings.countryColumn.ColumnName));
        }

        /// <summary>
        /// Get data and cache it
        /// </summary>
        /// <param name="exitCode"></param>
        /// <param name="tblKey"></param>
        /// <param name="createFunc"></param>
        /// <param name="loadFunc"></param>
        /// <param name="withDebug"></param>
        /// <param name="cacheTimeInSecs"></param>
        /// <returns></returns>
        internal static DataTable GetAndCacheData(out CommonTypes.ApiStatusCode exitCode, string tblKey,
                                                  CreateTableHandler createFunc, LoadDataHandler loadFunc = null,
                                                  int cacheTimeInSecs = constCacheTimeInSecs)
        {
            string key = string.Format("{0}.{1}", constCachePrefix, tblKey);
            DataTable tbl;
            if (DataCache.Libs.Get(key, out tbl, constCacheType))
            {
                exitCode = CommonTypes.ApiStatusCode.OK;
                return tbl;
            }
            tbl = createFunc();
            if (tbl == null)
            {
                CommonLibs.ErrorLibs.WriteLog("createFunc error 2");
                exitCode = CommonTypes.ApiStatusCode.Error;
                return null;
            }
            if (loadFunc == null)
            {
                exitCode = CommonTypes.ApiStatusCode.OK;
                return tbl;
            }
            CommonLibs.ErrorLibs.WriteDebugLog("Not found in cache : key = " + key);
            exitCode = loadFunc(tbl);
            if (exitCode != CommonTypes.ApiStatusCode.OK)
            {
                CommonLibs.ErrorLibs.WriteErrorLog("Load data error " + tblKey);
                return null;
            }
            DataCache.Logs.WriteDebugLog(string.Format("LoadCache : key {0} table {1}", key, tblKey));
            if (DataCache.Libs.Put(tbl, key, constCacheType, cacheTimeInSecs) == false)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(string.Format("PUT cache {0} error on {1}", constCacheType, key));
                exitCode = CommonTypes.ApiStatusCode.Error;
            }
            else exitCode = CommonTypes.ApiStatusCode.OK;
            return tbl;
        }

        /// <summary>
        /// Ensure that error does not stop others load behind the call
        /// </summary>
        /// <param name="tbl"></param>
        static CommonTypes.ApiStatusCode Load(DataTable tbl, string orderByStr)
        {
            try
            {
                string cmdText = string.Format("SELECT * FROM [{0}]", tbl.TableName);
                if (string.IsNullOrWhiteSpace(orderByStr) == false)
                    cmdText += Game.Consts.constCRLF + "ORDER BY " + orderByStr;
                lock (tbl)
                {
                    SqlCommand sqlCmd = new SqlCommand(cmdText);
                    if (EngineLibs.LoadFromSQL(tbl, sqlCmd) == false) return CommonTypes.ApiStatusCode.Error;
                    return CommonTypes.ApiStatusCode.OK;
                }
            }
            catch (Exception er)
            {
                CommonLibs.ErrorLibs.WriteErrorLog(er);
                return CommonTypes.ApiStatusCode.Error;
            }
        }
    }
}

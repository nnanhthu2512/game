﻿/********************************************************************************/
/* COPYRIGHT 										                            */
/* GlobeDR reserve all rights even in the event of industrial property rights.	*/
/* We reserve all rights of disposal such as copying and passing			    */
/* on to third parties. 										                */
/*													                            */
/* File name : dataEngine.cs 								                    */
/* Description : Common classes and functions supporting to retrieve/search     */
/* module.                                                                      */
/*													                            */
/* Developers : Dung Vu , Vietnam                                               */
/* History 											                            */
/* -----------------------------------------------------------------------------*/
/* Started on : 01 July 2014  							                        */
/* Revision : 1.1.0.3 									  	                    */
/* Changed by : Dung Vu									                        */
/* Change date : 12.Aug.2014 								                    */
/* Changes : 								                                    */
/* Reasons :  										                            */
/* -----------------------------------------------------------------------------*/

using CommonTypes;
using Game;
using System;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Xml;

namespace DataConfig
{
    [Serializable()]
    internal class ConfigInfo
    {
        public class ConnectionInfo
        {
            public RedisConnectionInfo Redis = new RedisConnectionInfo();
            public DbConnectionInfo Database = new DbConnectionInfo();
            public AzureStorageConnectionInfo Storage = new AzureStorageConnectionInfo();
        }
    }

    internal class AzureStorageConnectionInfo
    {
        public NetProtocol  Protocol = NetProtocol.HTTP;
        public string AccountName = null;
        public string AccountKey = null;
        public virtual string GetConnectionString()
        {
            //No space after ;
            return string.Format("DefaultEndpointsProtocol={0};AccountName={1};AccountKey={2}", this.Protocol.ToString().ToLower(), this.AccountName, this.AccountKey);
        }
    }

    internal class BaseConnectionInfo
    {
        public string Address = null;
        public ushort? Port = null;
        public string Account = null;
        public string Password = null;
        public ushort? TimeoutInSecs = null;

        public virtual string GetConnectionString()
        {
            return null;
        }
    }

    internal class DbConnectionInfo : BaseConnectionInfo
    {
        System.Data.SqlClient.SqlConnectionStringBuilder sqlConnBuilder = new SqlConnectionStringBuilder();
        public string Database = null;
        public void SetTest()
        {
            this.Account = "";
            this.Database = "";
            this.Password = "";
            this.Address = "";
        }
        public override string GetConnectionString()
        {
            sqlConnBuilder.UserID = this.Account;
            sqlConnBuilder.Password = this.Password;
            sqlConnBuilder.InitialCatalog = this.Database;
            sqlConnBuilder.DataSource = this.Address;
            sqlConnBuilder.ConnectTimeout = (int)this.TimeoutInSecs;
            return sqlConnBuilder.ConnectionString;
        }
    }

    internal class RedisConnectionInfo : BaseConnectionInfo
    {
        public RedisConnectionInfo() { }

        public RedisConnectionInfo(string address, string pwd, ushort timeoutInSec = 60)
        {
            this.Address = address;
            this.Password = pwd;
            this.TimeoutInSecs = timeoutInSec;
        }

        public bool UseSSL = false;
        public override string GetConnectionString()
        {
            string address = this.Address.Trim() + (this.Port == null ? "" : ":" + this.Port.ToString());
            return String.Format("{0},ssl={2},password={1},ConnectTimeout={3}", address, this.Password, this.UseSSL ? "true" : "false", this.TimeoutInSecs * 1000);
        }
    }

    public class Configuration
    {
        private static string _sysDbConnectionStr = null;
        private static string _sysRedisConnectionStr = null;
        private static string _sysStorageConnectionStr = null;

        internal static string sysDbConnectionStr
        {
            get
            {
                if (_sysDbConnectionStr == null) SetConnectionStr();
                return _sysDbConnectionStr;
            }
        }

        internal static string sysStorageConnectionStr
        {
            get
            {
                if (_sysStorageConnectionStr == null) SetConnectionStr();
                return _sysStorageConnectionStr;
            }
        }

        private static bool SetConnectionStr()
        {
            string fileName = GetSystemConfigFileName();
            //WriteLog(String.Format("Use config file {0} : ", fileName));

            XmlDocument xmlDoc = CommonLibs.xmlLibs.OpenXML(fileName);
            if (xmlDoc == null)
            {
                //WriteLog(String.Format("Config file {0} error !", fileName));
                return false;
            }
            ConfigInfo.ConnectionInfo info = new ConfigInfo.ConnectionInfo();
            if (Configuration.GetConectionInfo(info.Database, xmlDoc, 0) == false)
            {
                //WriteLog(String.Format("Do not find Database connection in config file {0} !", fileName));
                return false;
            }
            _sysDbConnectionStr = info.Database.GetConnectionString();

            if (Configuration.GetConectionInfo(info.Storage, xmlDoc, 0) == false)
            {
                //WriteLog(String.Format("Do not find Storage connection in config file {0} !", fileName));
                return false;
            }
            _sysStorageConnectionStr = info.Storage.GetConnectionString();

            if (Configuration.GetConectionInfo(info.Redis, xmlDoc, 0) == false)
            {
                //WriteLog(String.Format("Do not find Redis connection in config file {0} !", fileName));
                return false;
            }
            _sysRedisConnectionStr = info.Redis.GetConnectionString();
            return true;
        }

        private static string GetSystemConfigFileName()
        {
            return CommonLibs.FileSystem.Combine(CommonLibs.FileSystem.GetRootFolderPath(), Consts.SystemConf);
        }

        private static bool withEncryption = true;
        private static bool GetConectionInfo(DbConnectionInfo info, XmlDocument xmlDoc, int connId)
        {
            StringCollection aFields = new StringCollection();
            aFields.Add("Server");
            aFields.Add("Database");
            aFields.Add("Account");
            aFields.Add("Password");
            aFields.Add("Timeout");

            if (!GetConfiguration(xmlDoc, "DBCONNECTION", "Config" + connId.ToString(), aFields))
            {
                info.SetTest();
                CommonLibs.ErrorLibs.ShowErrorMessage("dbConnectionError");
                return true;
            }
            info.Address = aFields[0].ToString();
            info.Database = aFields[1].ToString();
            info.Account = aFields[2].ToString();
            info.Password = aFields[3].ToString();
            ushort to = 0;
            if (ushort.TryParse(aFields[4].ToString(), out to)) info.TimeoutInSecs = to;
            return true;
        }
        private static bool GetConectionInfo(AzureStorageConnectionInfo info, XmlDocument xmlDoc, int connId)
        {
            StringCollection aFields = new StringCollection();
            aFields.Add("Protocol");
            aFields.Add("AccountName");
            aFields.Add("AccountKey");

            if (GetConfiguration(xmlDoc, "AzureCloudStorage", "Config" + connId.ToString(), aFields) == false) return false;
            info.Protocol = (NetProtocol)int.Parse(aFields[0]);
            info.AccountName = aFields[1].ToString();
            info.AccountKey = aFields[2].ToString();
            return true;
        }
        private static bool GetConectionInfo(RedisConnectionInfo info, XmlDocument xmlDoc, int connId)
        {
            StringCollection aFields = new StringCollection();
            aFields.Add("Address");
            aFields.Add("Port");
            aFields.Add("Key");
            aFields.Add("TimeOut");

            if (GetConfiguration(xmlDoc, "AzureRedis", "Config" + connId.ToString(), aFields) == false) return false;
            info.Address = aFields[0];
            info.Port = ushort.Parse(aFields[1]);
            info.Password = aFields[2];
            info.TimeoutInSecs = ushort.Parse(aFields[3]);
            return true;
        }

        private static bool GetConfiguration(XmlDocument xmlDoc, string type, string subType, StringCollection aFields)
        {
            return GetConfiguration(xmlDoc, GetNodeNames(type, subType), aFields);
        }

        private static bool GetConfiguration(XmlDocument XMLDoc, string[] nodes, StringCollection aFields)
        {
            string xmlPath = CommonLibs.xmlLibs.MakeXmlPath(nodes);
            bool retVal = true;
            for (int idx = 0; idx < aFields.Count; idx++)
            {
                aFields[idx] = CommonLibs.xmlLibs.GetElement(XMLDoc, xmlPath, aFields[idx], withEncryption);
                if (aFields[idx] == null)
                {
                    retVal = false;
                    aFields[idx] = "";
                }
            }
            return retVal;
        }

        private static string[] GetNodeNames(string type, string subType)
        {
            if (CommonLibs.StringLibs.IsNullOrWhiteSpace(subType))
            {
                if (CommonLibs.StringLibs.IsNullOrWhiteSpace(type))
                    return new string[] { Consts.constXmlRootElement };
                return new string[] { Consts.constXmlRootElement, type };
            }
            return new string[] { Consts.constXmlRootElement, type, subType };
        }
    }
}

﻿using DataAccess;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Game
{
    public class BO
    {
        //[Serializable]
        //public class Button
        //{
        //    public int X, Y;
        //    public string ImgUrl;
        //    public int Width, Height;
        //}

        public class Scene
        {
            public int? Id;
            public string ImgUrl;

            internal int WidthOrigin, HeightOrigin;
            internal float WidthPercent, HeightPercent;

            //public List<BO.Button> ButtonList = new List<Button>();

            public void CopyFrom(DBCache.Scene scene)
            {
                this.Id = scene.Id;

                //foreach (var item in scene.ButtonList)
                //    this.ButtonList.Add(item);
            }

            public void CopyFrom(DBCache.SceneImg scene, int widthScreen, int heightScreen)
            {
                CommonLibs.ErrorLibs.WriteDebugLog("CopyFrom Scene ImgUrl: " + scene.ImgUrl);
                this.ImgUrl = scene.ImgUrl;
                this.WidthOrigin = scene.WidthOrigin;
                this.HeightOrigin = scene.HeightOrigin;

                this.WidthPercent = (float)widthScreen / Settings.WidthGiftSceneDefault;
                this.HeightPercent = (float)heightScreen / Settings.HeightGiftSceneDefault;
            }

        }

        [Serializable]
        public class Gift
        {
            public string ImgUrl;
            public string LogoUrl;
            public string Description;
            public string Name;
            public string Address;
            public string Website;
            public string Email;
            public string Phone;

            public void CopyFrom(Gift gift)
            {
                this.ImgUrl = gift.ImgUrl;
                this.LogoUrl = gift.LogoUrl;
                this.Description = gift.Description;
                this.Name = gift.Name;
                this.Address = gift.Address;
                this.Website = gift.Website;
                this.Email = gift.Email;
                this.Phone = gift.Phone;
            }
        }

        [Serializable]
        public class SpotLocation
        {
            public int X, Y;
            public int Width, Height;

            public void CopyFrom(SpotLocation spot)
            {
                this.X = spot.X;
                this.Y = spot.Y;
                this.Width = spot.Width;
                this.Height = spot.Height;
            }
        }

        [Serializable]
        public class GiftSpot
        {
            public SpotLocation Location = new SpotLocation();
            public Gift Gift = new Gift();

            public void CopyFrom(GiftSpot info)
            {
                this.Location.CopyFrom(info.Location);
                this.Gift.CopyFrom(info.Gift);
            }
        }

        public class UserSubmit : CommonAPI.BO.Game.UserSubmit
        {
            public void CopyFrom(game.myGame.UserSubmitRow row)
            {
                this.Id = row.Id;
                this.FirstName = row.FirstName;
                this.LastName = row.LastName;
                this.EmailOrPhone = row.EmailOrPhone;
                this.SubmitGifts = row.IsSubmitGiftsNull() ? null : (short?)row.SubmitGifts;
                this.PlayCounts = row.IsPlayCountsNull() ? null : (short?)row.PlayCounts;
                this.CorrectAnswer = row.IsCorrectAnswerNull() ? null : (short?)row.CorrectAnswer;
                this.PlayTime = row.IsPlayTimeNull() ? null : (double?)row.PlayTime;
                this.Country = row.IsCountryNull() ? null : row.Country;
                this.OnDate = row.OnDate;
            }

        }

        public class Country : CommonTypes.BaseObject
        {
            public virtual string Code { get; set; }
            public virtual string Name { get; set; }
            public virtual string ShortName { get; set; }
            public virtual string PostCode { get; set; }
            public Country() : base() { }

            public Country(string code = null, string name = null, string shortName = null) : base()
            {
                this.Code = code;
                this.Name = name;
                this.ShortName = shortName;
            }

        }
    }

    public class DBCache
    {
        private static string KeyScene = "Game_KeyScene_";
        private static string KeySpot = "Game_KeySpot";
        private static string KeyGift = "Game_KeyGift";

        [Serializable]
        public class SceneImg
        {
            public string ImgUrl;
            internal int WidthOrigin, HeightOrigin;
        }

        [Serializable]
        public class Scene
        {
            public int? Id;
            public List<SceneImg> SceneImgList = new List<SceneImg>();
            //public List<BO.Button> ButtonList = new List<BO.Button>();
        }

        internal static Scene GetScene(Consts.NoScene noScene)
        {
            //Get from cache
            string cacheKey = KeyScene + noScene.ToString();
            Scene cacheData = null;
            bool status = DataCache.Libs.Get(cacheKey, out cacheData);
            if (status == false)
            {
                CommonLibs.ErrorLibs.WriteDebugLog("GetScene No cache");
                //Put to cache
                cacheData = new Scene();
                cacheData = BL.Distribution.GetScene(noScene);
                DataCache.Libs.Put(cacheData, cacheKey);
            }
            else
            {
                CommonLibs.ErrorLibs.WriteDebugLog("GetScene Have cache");
            }

            return cacheData;
        }

        internal static List<BO.SpotLocation> GetSpot()
        {
            //Get from cache
            List<BO.SpotLocation> cacheData = null;
            if (DataCache.Libs.Get(KeySpot, out cacheData) == false)
            {
                //Put to cache
                cacheData = new List<BO.SpotLocation>();
                cacheData = BL.Distribution.GetSpot();
                DataCache.Libs.Put(cacheData, KeySpot);
            }

            return cacheData;
        }

        internal static List<BO.Gift> GetGift()
        {
            //Get from cache
            List<BO.Gift> cacheData = null;
            if (DataCache.Libs.Get(KeyGift, out cacheData) == false)
            {
                //Put to cache
                cacheData = new List<BO.Gift>();
                cacheData = BL.Distribution.GetGift();
                DataCache.Libs.Put(cacheData, KeyGift);
            }

            return cacheData;
        }
    }

    public static class BL
    {
        public static class Distribution
        {
            /// <summary>
            /// Get SceneImg for each devices depends on XElement element 
            /// </summary>
            /// <param name="list">List result</param>
            /// <param name="element">Element in xml file</param>
            private static void GetScene(List<DBCache.SceneImg> list, IEnumerable<XElement> element)
            {
                foreach (var btn in element)
                {
                    DBCache.SceneImg sceneImg = new DBCache.SceneImg();
                    sceneImg.ImgUrl = HttpUtility.UrlEncode(
                                        CommonLibs.FileSystem.GetFile(btn.Element("imgUrl").Value,
                                                                        CommonTypes.SignatureImportance.None,
                                                                        CommonTypes.SystemFolderType.Image,
                                                                        CommonTypes.SystemFolderImg.Background));
                    sceneImg.WidthOrigin = int.Parse(btn.Element("width").Value);
                    sceneImg.HeightOrigin = int.Parse(btn.Element("height").Value);

                    list.Add(sceneImg);
                }
            }

            /// <summary>
            /// Get Scene from xml file. Data returned will save to cache
            /// </summary>
            /// <param name="noScene"></param>
            /// <returns></returns>
            internal static DBCache.Scene GetScene(Consts.NoScene noScene)
            {
                DBCache.Scene scene = new DBCache.Scene();
                string fileName = CommonLibs.FileSystem.GetFile(Settings.SceneFileName, CommonTypes.SignatureImportance.Forever, CommonTypes.SystemFolderType.Data);

                using (var reader = XmlReader.Create(fileName))
                {
                    var xelement = XElement.Load(reader);
                    var scenes = xelement.Elements();
                    foreach (var sceneItem in scenes)
                    {
                        if (int.Parse(sceneItem.Element("id").Value) == (int)noScene)
                        {
                            scene.Id = int.Parse(sceneItem.Element("id").Value);

                            //Scene for all devices
                            GetScene(scene.SceneImgList, sceneItem.Elements("mobileLandscape"));
                            GetScene(scene.SceneImgList, sceneItem.Elements("mobilePortrait"));
                            GetScene(scene.SceneImgList, sceneItem.Elements("ipadLandscape"));
                            GetScene(scene.SceneImgList, sceneItem.Elements("ipadPortrait"));
                            GetScene(scene.SceneImgList, sceneItem.Elements("desktop"));

                            //Button list
                            //var btns = sceneItem.Elements("button");
                            //foreach (var btn in btns)
                            //{
                            //    BO.Button btnTemp = new BO.Button();
                            //    btnTemp.ImgUrl = HttpUtility.UrlEncode(
                            //                        CommonLibs.FileSystem.GetFile(btn.Element("imgUrl").Value,
                            //                                                        CommonTypes.SignatureImportance.None,
                            //                                                        CommonTypes.SystemFolderType.Image));
                            //    btnTemp.X = int.Parse(btn.Element("x").Value);
                            //    btnTemp.Y = int.Parse(btn.Element("y").Value);

                            //    btnTemp.Width = int.Parse(btn.Element("width").Value);
                            //    btnTemp.Height = int.Parse(btn.Element("height").Value);

                            //    scene.ButtonList.Add(btnTemp);
                            //}
                            break;
                        }
                    }
                    reader.Close();
                }
                return scene;
            }

            /// <summary>
            /// Get Scene from cache
            /// </summary>
            /// <param name="list"></param>
            /// <param name="widthScreen"></param>
            /// <param name="heightScreen"></param>
            internal static void GetSceneByScreen(BO.Scene scene, List<DBCache.SceneImg> list, int widthScreen, int heightScreen)
            {
                float widthPerHeight = (float)widthScreen / heightScreen;

                DBCache.SceneImg closest = list.Aggregate((x, y) => Math.Abs(((float)x.WidthOrigin / x.HeightOrigin) - widthPerHeight) < Math.Abs(((float)y.WidthOrigin / y.HeightOrigin) - widthPerHeight) ? x : y);
                scene.CopyFrom(closest, widthScreen, heightScreen);
            }

            /// <summary>
            /// Get information of a scene in the game
            /// </summary>
            /// <param name="no"></param>
            /// <returns></returns>
            public static BO.Scene GetScene(Consts.NoScene noScene, int widthScreen, int heightScreen)
            {
                try
                {
                    BO.Scene scene = new BO.Scene();

                    DBCache.Scene sceneCache = DBCache.GetScene(noScene);
                    scene.CopyFrom(sceneCache);
                    GetSceneByScreen(scene, sceneCache.SceneImgList, widthScreen, heightScreen);

                    //foreach (var btnTemp in scene.ButtonList)
                    //{
                    //    btnTemp.X = (int)Math.Ceiling(btnTemp.X * scene.WidthPercent);
                    //    btnTemp.Y = (int)Math.Ceiling(btnTemp.Y * scene.HeightPercent);

                    //    btnTemp.Width = (int)Math.Ceiling(btnTemp.Width * scene.WidthPercent);
                    //    btnTemp.Height = (int)Math.Ceiling(btnTemp.Height * scene.HeightPercent);
                    //}
                    return scene;
                }
                catch (Exception er)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(er);
                    return new BO.Scene();
                }

            }

            /// <summary>
            /// Get spot from xml file
            /// </summary>
            /// <returns></returns>
            internal static List<BO.SpotLocation> GetSpot()
            {
                List<BO.SpotLocation> result = new List<BO.SpotLocation>();

                string fileName = CommonLibs.FileSystem.GetFile(Settings.SpotFileName, CommonTypes.SignatureImportance.Forever, CommonTypes.SystemFolderType.Data);
                using (var reader = XmlReader.Create(fileName))
                {
                    XElement xelement = XElement.Load(reader);
                    IEnumerable<XElement> spots = xelement.Elements();
                    foreach (var spotItem in spots)
                    {
                        BO.SpotLocation spot = new BO.SpotLocation();
                        spot.X = int.Parse(spotItem.Element("x").Value);
                        spot.Y = int.Parse(spotItem.Element("y").Value);

                        spot.Width = int.Parse(spotItem.Element("width").Value);
                        spot.Height = int.Parse(spotItem.Element("height").Value);
                        result.Add(spot);
                    }
                    reader.Close();
                }
                return result;
            }

            /// <summary>
            /// Get gift from xml file
            /// </summary>
            /// <returns></returns>
            internal static List<BO.Gift> GetGift()
            {
                List<BO.Gift> result = new List<BO.Gift>();

                string fileName = CommonLibs.FileSystem.GetFile(Settings.GiftFileName, CommonTypes.SignatureImportance.Forever, CommonTypes.SystemFolderType.Data);
                using (var reader = XmlReader.Create(fileName))
                {
                    XElement xelement = XElement.Load(reader);
                    IEnumerable<XElement> gifts = xelement.Elements();

                    for (var i = 0; i < gifts.Count(); i++)
                    {
                        BO.Gift giftSpot = new BO.Gift();

                        giftSpot.Name = gifts.ElementAt(i).Element("name").Value;
                        giftSpot.Description = gifts.ElementAt(i).Element("description").Value;
                        giftSpot.Address = gifts.ElementAt(i).Element("address").Value;
                        giftSpot.Website = gifts.ElementAt(i).Element("website").Value;
                        giftSpot.Email = gifts.ElementAt(i).Element("email").Value;
                        giftSpot.Phone = gifts.ElementAt(i).Element("phone").Value;

                        giftSpot.ImgUrl = HttpUtility.UrlEncode(
                                                        CommonLibs.FileSystem.GetFile(gifts.ElementAt(i).Element("imgUrl").Value,
                                                                                        CommonTypes.SignatureImportance.None,
                                                                                        CommonTypes.SystemFolderType.Image,
                                                                                        CommonTypes.SystemFolderImg.Gift)
                                                                                        );
                        giftSpot.LogoUrl = HttpUtility.UrlEncode(
                                                        CommonLibs.FileSystem.GetFile(gifts.ElementAt(i).Element("logo").Value,
                                                                                        CommonTypes.SignatureImportance.None,
                                                                                        CommonTypes.SystemFolderType.Image,
                                                                                        CommonTypes.SystemFolderImg.Logo)
                                                                                        );
                        result.Add(giftSpot);
                    }
                    reader.Close();
                }
                return result;
            }

            /// <summary>
            /// Get gift list for user to play.
            /// </summary>
            /// <returns></returns>
            public static List<BO.GiftSpot> GetGift(Consts.NoScene noScene, int widthScreen, int heightScreen, out short? trueGift)
            {
                try
                {
                    List<BO.GiftSpot> result = new List<BO.GiftSpot>();

                    //Get Scene
                    BO.Scene scene = new BO.Scene();
                    DBCache.Scene sceneCache = DBCache.GetScene(noScene);
                    GetSceneByScreen(scene, sceneCache.SceneImgList, widthScreen, heightScreen);

                    //Get spot
                    List<BO.SpotLocation> spotList = DBCache.GetSpot();

                    //Get gift
                    List<BO.Gift> giftList = DBCache.GetGift();

                    Random random = new Random();

                    Random rnd = new Random();
                    short totalGift = Convert.ToInt16(rnd.Next(Settings.MinTotalGifts, Settings.MaxTotalGifts + 1));
                    trueGift = totalGift;

                    if (spotList.Count < totalGift)
                    {
                        CommonLibs.ErrorLibs.WriteErrorLog("SpotList is shorter than TotalGifts");
                        trueGift = null;
                        return new List<BO.GiftSpot>();
                    }

                    //string tempStr = "";
                    while (totalGift - result.Count != 0)
                    {
                        BO.GiftSpot giftSpot = new BO.GiftSpot();
                        while (spotList.Any())
                        {
                            var randomIdx = random.Next(0, spotList.Count());

                            spotList[randomIdx].X = (int)Math.Ceiling(spotList[randomIdx].X * scene.WidthPercent);
                            spotList[randomIdx].Y = (int)Math.Ceiling(spotList[randomIdx].Y * scene.HeightPercent);


                            if (spotList[randomIdx].Width == 15)
                            {
                                CommonLibs.ErrorLibs.WriteDebugLog("15");
                            }
                            //Get weight/height og gift 
                            //e.g MobilePortrait(640 x 920): Gift(15 x 15) on Scene default (1170 x 650) => percent size (0.547, 1.415)
                            // => real gift (9, 9)
                            spotList[randomIdx].Width = (int)Math.Ceiling(spotList[randomIdx].Width * scene.WidthPercent);
                            //spotList[randomIdx].Height = (int)Math.Ceiling(spotList[randomIdx].Height * scene.WidthPercent);

                            //Check if gift size is too small?
                            spotList[randomIdx].Height = spotList[randomIdx].Width = spotList[randomIdx].Width < Settings.MinGiftSize ? Settings.MinGiftSize : spotList[randomIdx].Width;
                            //spotList[randomIdx].Height = spotList[randomIdx].Height < Settings.MinGiftSize ? Settings.MinGiftSize : spotList[randomIdx].Width;

                            giftSpot.Location.CopyFrom(spotList[randomIdx]);

                            spotList.RemoveAt(randomIdx);
                            break;
                        }

                        while (giftList.Any())
                        {
                            var randomIdx = random.Next(0, giftList.Count());
                            giftSpot.Gift.CopyFrom(giftList[randomIdx]);
                            giftList.RemoveAt(randomIdx);
                            break;
                        }

                        ////Log
                        //tempStr += String.Format("x: {0}, y: {1}, W: {2}, H: {3}", giftSpot.Location.X, giftSpot.Location.Y, giftSpot.Location.Width, giftSpot.Location.Height);
                        result.Add(giftSpot);

                        if (giftList.Count() == 0)
                            giftList = DBCache.GetGift();
                    }

                    ////log
                    //string temp = "";
                    //foreach (var item in result)
                    //{
                    //    //temp += String.Format("x: {0}, y: {1}, W: {2}, H: {3}", item.Location.X, item.Location.Y, item.Location.Width, item.Location.Height);
                    //    temp += String.Format("{0} ", item.Gift.Text);
                    //}

                    return result;
                }
                catch (Exception er)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(er);
                    trueGift = null;
                    return new List<BO.GiftSpot>();
                }

            }

            /// <summary>
            /// Submit user anwser
            /// </summary>
            /// <param name="info"></param>
            /// <returns></returns>
            public static bool UserSubmit(BO.UserSubmit info)
            {
                try
                {
                    using (ReliableSqlConnection conn = EngineLibs.OpenConnection())
                    {

                        //Check info is existed
                        string tempStr = info.EmailOrPhone.IndexOf('@') > 0 ? info.EmailOrPhone.Substring(0, info.EmailOrPhone.IndexOf('@')) : info.EmailOrPhone;
                        if (String.IsNullOrWhiteSpace(info.FirstName))
                        {
                            info.FirstName = tempStr;
                        }
                        if (String.IsNullOrWhiteSpace(info.LastName))
                        {
                            info.LastName = tempStr;
                        }

                        //CommonLibs.ErrorLibs.WriteDebugLog(String.Format("UserSubmit: {0} - {1} - {2} - NoGifts: {3} - TrueNoGifts: {4} - TimeFinish: {5}",
                        //    info.FirstName , info.LastName, info.Country, info.NoGifts, info.TrueNoGifts, info.TimeFinish));

                        info.EmailOrPhone = ParseIdentity(info.EmailOrPhone, info.Country);

                        var result = DA.UserSubmit.CheckUserExist(info, conn);
                        if (result)
                        {
                            info.PlayCounts += 1;
                            result = DA.UserSubmit.Update(info, info.Id.Value, conn);
                        }
                        else
                        {
                            info.PlayCounts = 1;
                            result = DA.UserSubmit.AddNew(info, conn);
                        }

                        if (result == false) return result;

                        //Get rank of submit
                        Engine.UserSubmit engine = new Engine.UserSubmit();
                        info.Rank = engine.GetRangeOfSubmit(info, conn);

                        //CommonLibs.ErrorLibs.WriteDebugLog("rank: " + info.Rank);
                        //Sent result to GDR
                        if (ExtendCtrl.CreateGdrUser(info) == false) return false;

                        EngineLibs.CloseConnection(conn);
                        return result;
                    }
                }
                catch (Exception er)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(er);
                    return false;
                }

            }

            public static string ParseIdentity(string identity, string country)
            {
                if (string.IsNullOrWhiteSpace(identity)) return null;
                identity = identity.Trim();
                //Check if email
                if (ValidateLibs.ValidateEmail(identity))
                {
                    return identity;
                }

                //Check if phone
                DataAccess.PhoneNo phoneInfo = new PhoneNo();
                if (phoneInfo.Parse(identity, country))
                {
                    string ShortPhoneNo = phoneInfo.ShortPhoneNo;
                    return AppLibs.MakeLoginId(phoneInfo.CountryCode, phoneInfo.ShortPhoneNo);
                    //return true;
                }

                return null;
            }



            public static BO.UserSubmit LoadUser(string emailOrPhone, string country)
            {
                try
                {
                    BO.UserSubmit result = new BO.UserSubmit();

                    emailOrPhone = ParseIdentity(emailOrPhone, country);
                    Engine.UserSubmit engine = new Engine.UserSubmit();
                    engine.Country = country;
                    engine.EmailOrPhone = emailOrPhone;


                    game.myGame.UserSubmitDataTable tbl = new game.myGame.UserSubmitDataTable();
                    engine.Load(tbl);

                    if (tbl.Count == 0) return null;
                    result.Country = country;
                    result.EmailOrPhone = emailOrPhone;
                    result.FirstName = tbl[0].FirstName;
                    result.LastName = tbl[0].LastName;
                    return result;
                }
                catch (Exception er)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(er);
                    return null;
                }
            }

            public static List<BO.Country> GetCountryList(CommonTypes.SysLanguage lang)
            {
                try
                {
                    List<BO.Country> list = new List<BO.Country>();
                    if (DataAccess.DbCache.CountryTbl == null) return null;
                    lock (DataAccess.DbCache.CountryTbl)
                    {
                        DataView myView = new DataView(DataAccess.DbCache.CountryTbl);
                        myView.RowFilter = string.Format("{0}='{1}'",
                                                        DataAccess.DbCache.CountryTbl.languageColumn.ColumnName,
                                                        CommonLibs.SysLibs.GetCultureCode(lang));
                        myView.Sort = DataAccess.DbCache.CountryTbl.descriptionColumn.ColumnName;
                        foreach (DataRowView row in myView)
                        {
                            game.myGame.countryRow infoRow = (game.myGame.countryRow)row.Row;
                            BO.Country item = new BO.Country(infoRow.code, infoRow.description);
                            game.myGame.sysCountrySettingsRow settingRow = DataAccess.DbCache.CountrySettings.FindBycountry(item.Code);
                            if (settingRow != null)
                            {
                                item.PostCode = settingRow.postCode;
                                if (string.IsNullOrWhiteSpace(item.PostCode) == false)
                                    item.PostCode = string.Format("(+{0})", item.PostCode.Trim());
                                item.ShortName = item.Name + " " + item.PostCode;
                            }
                            list.Add(item);
                        }
                        return list;
                    }
                }
                catch (Exception er)
                {
                    CommonLibs.ErrorLibs.WriteErrorLog(er);
                    return null;
                }
            }

        }

        public static class ValidateLibs
        {
            /// <summary>
            /// Validate email
            /// </summary>
            /// <param name="email"></param>
            /// <returns></returns>
            public static bool ValidateEmail(string email)
            {
                if (string.IsNullOrWhiteSpace(email)) return false;
                string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(strRegex);
                return re.IsMatch(email) && email.Length <= Consts.MaxLengthEmail;
            }

            /// <summary>
            /// Validate phone no
            /// </summary>
            /// <param name="number"></param>
            /// <returns></returns>
            public static bool ValidatePhoneNo(string phoneNo)
            {
                if (string.IsNullOrWhiteSpace(phoneNo)) return false;
                if (Regex.IsMatch(phoneNo, @"^\d+$") == false) return false;
                return phoneNo.Length >= Consts.MinLengthPhoneNo && phoneNo.Length <= Consts.MaxLengthPhoneNo;
            }

            public static bool IsValidSpeChar(string str)
            {
                if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(Settings.sysExcludeSpecialChars)) return true;
                foreach (char c in Settings.sysExcludeSpecialChars)
                {
                    if (str.IndexOf(c.ToString(), StringComparison.OrdinalIgnoreCase) >= 0) return false;
                }
                return true;
            }
        }
    }

    internal static class DA
    {
        internal static class UserSubmit
        {

            internal static void BuildSqlFlds(BO.UserSubmit info, SqlCommand sqlCommand, StringCollection fldList, QueryType queryType)
            {
                string fldName;
                string alias = EngineLibs.GetTableAlias(TblNames.UserSubmit);
                // Id is auto-generated number and cannot be updated
                if (info.Id != null && ((short)queryType & (short)QueryType.Select) > 0)
                {
                    fldName = DbCache.myGameDS.UserSubmit.IdColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.Id);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }

                if (string.IsNullOrWhiteSpace(info.EmailOrPhone) == false)
                {
                    fldName = DbCache.myGameDS.UserSubmit.EmailOrPhoneColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.EmailOrPhone);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }

                if (string.IsNullOrWhiteSpace(info.FirstName) == false)
                {
                    fldName = DbCache.myGameDS.UserSubmit.FirstNameColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.FirstName);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }
                if (string.IsNullOrWhiteSpace(info.LastName) == false)
                {
                    fldName = DbCache.myGameDS.UserSubmit.LastNameColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.LastName);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }

                if (info.SubmitGifts != null)
                {
                    fldName = DbCache.myGameDS.UserSubmit.SubmitGiftsColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.SubmitGifts);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }

                if (info.CorrectAnswer != null)
                {
                    fldName = DbCache.myGameDS.UserSubmit.CorrectAnswerColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.CorrectAnswer);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }

                if (info.PlayCounts != null)
                {
                    fldName = DbCache.myGameDS.UserSubmit.PlayCountsColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.PlayCounts);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }
                if (info.Country != null)
                {
                    fldName = DbCache.myGameDS.UserSubmit.CountryColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.Country);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }
                if (info.OnDate != null)
                {
                    fldName = DbCache.myGameDS.UserSubmit.OnDateColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.OnDate);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }

                if (info.PlayTime != null)
                {
                    fldName = DbCache.myGameDS.UserSubmit.PlayTimeColumn.ColumnName;
                    if (sqlCommand != null) EngineLibs.AddSqlParam(sqlCommand, fldName, info.PlayTime);
                    EngineLibs.Add(fldList, fldName, alias, queryType);
                }
            }

            internal static bool ValidateInfo(BO.UserSubmit info)
            {
                if (String.IsNullOrWhiteSpace(info.FirstName) || String.IsNullOrWhiteSpace(info.LastName) ||
                    String.IsNullOrWhiteSpace(info.EmailOrPhone) || info.SubmitGifts == null || info.CorrectAnswer == null)
                    return false;
                return true;
            }

            internal static bool AddNew(BO.UserSubmit info, ReliableSqlConnection conn = null)
            {
                if (info.Validate() == false)
                    return false;

                if (info.OnDate == null) info.OnDate = DateTime.Now;

                //Add new promotion
                SqlCommand sqlCommand = new SqlCommand();
                StringCollection fldList = new StringCollection();
                BuildSqlFlds(info, sqlCommand, fldList, QueryType.Insert);
                if (fldList.Count == 0) return false;

                // Build sql
                if (sqlCommand.Parameters.Count == 0) return false;
                string fldStr = CommonLibs.StringLibs.ToString(fldList, ",", "", "");
                string valueStr = CommonLibs.StringLibs.ToString(fldList, ",", "@", "");
                sqlCommand.CommandText = String.Format("INSERT [{0}] ({1}){2}VALUES({3})",
                                                       EngineLibs.GetTableName(TblNames.UserSubmit), fldStr, Consts.constCRLF, valueStr) +
                                                       Consts.constCRLF +
                                                       String.Format("SET @{0}=SCOPE_IDENTITY()", DbCache.myGameDS.UserSubmit.IdColumn.ColumnName);

                sqlCommand.Parameters.Add(DbCache.myGameDS.UserSubmit.IdColumn.ColumnName, SqlDbType.Int).Direction = ParameterDirection.Output;
                if (EngineLibs.ExcuteNonQuerySQL(sqlCommand, conn) <= 0) return false;
                info.Id = (int)sqlCommand.Parameters[DbCache.myGameDS.UserSubmit.IdColumn.ColumnName].Value;
                EngineLibs.CloseConnection(conn);
                return true;

            }

            internal static bool Update(BO.UserSubmit info, int userId, ReliableSqlConnection conn = null)
            {
                if (info.OnDate == null) info.OnDate = DateTime.Now;
                string filter = "";
                SqlCommand sqlCommand = new SqlCommand();
                filter += (String.IsNullOrWhiteSpace(filter) ? "" : " AND ") + String.Format("{0}={1}",
                                                                                                DbCache.myGameDS.UserSubmit.IdColumn.ColumnName, userId);


                if (String.IsNullOrWhiteSpace(filter)) return false;

                StringCollection fldList = new StringCollection();
                BuildSqlFlds(info, sqlCommand, fldList, QueryType.Update);

                if (sqlCommand.Parameters.Count == 0) return false;

                string fldStr = CommonLibs.StringLibs.ToString(fldList, ",", "", "");
                sqlCommand.CommandText = String.Format("UPDATE {0} SET {1} WHERE {2}", EngineLibs.GetTableName(TblNames.UserSubmit), fldStr, filter);
                if (EngineLibs.ExcuteNonQuerySQL(sqlCommand, conn) <= 0) return false;

                return true;
            }

            internal static bool CheckUserExist(BO.UserSubmit info, ReliableSqlConnection conn = null)
            {
                Engine.UserSubmit engine = new Engine.UserSubmit();
                engine.Country = info.Country;
                engine.EmailOrPhone = info.EmailOrPhone;

                game.myGame.UserSubmitDataTable tbl = new game.myGame.UserSubmitDataTable();
                engine.Load(tbl, conn);

                if (tbl.Count == 0) return false;
                info.Id = tbl[0].Id;
                info.PlayCounts = tbl[0].IsPlayCountsNull() ? 1 : (short?)tbl[0].PlayCounts;
                return true;
            }

        }
    }

    internal class Criteria
    {
        internal class UserSubmit
        {
            public string Country = null;
            public string EmailOrPhone = null;

            internal void CopyFrom(UserSubmit info)
            {
                this.Country = info.Country;
                this.EmailOrPhone = info.EmailOrPhone;
            }
        }
    }

    internal class Engine
    {
        internal class UserSubmit : Criteria.UserSubmit
        {
            internal EngineInfo EngineInfo = new EngineInfo();

            internal void Build(string paramPrefix = null)
            {
                this.EngineInfo.Reset();
                BuildFilter(this.EngineInfo, paramPrefix);
            }

            private void BuildFilter(EngineInfo engineInfo, string paramPrefix)
            {
                string fldName, varName, tmp;

                if (String.IsNullOrWhiteSpace(this.Country) == false)
                {
                    varName = DbCache.myGameDS.UserSubmit.CountryColumn.ColumnName;
                    fldName = EngineLibs.GetFieldName(TblNames.UserSubmit, varName);

                    varName = EngineLibs.MakeParamVarName(paramPrefix, varName);
                    EngineLibs.AddSqlParam(this.EngineInfo.SqlCommand, varName, this.Country);
                    engineInfo.AddFilter(String.Format("{0}=@{1}", fldName, varName));
                }


                if (String.IsNullOrWhiteSpace(this.EmailOrPhone) == false)
                {
                    SearchOptions searchOption;
                    varName = DbCache.myGameDS.UserSubmit.EmailOrPhoneColumn.ColumnName;
                    fldName = EngineLibs.GetFieldName(TblNames.UserSubmit, varName);
                    varName = EngineLibs.MakeParamVarName(paramPrefix, varName);
                    searchOption = EngineInfo.GetSearchOption(TblNames.UserSubmit, varName, DataAccess.SearchOptions.Exact);
                    EngineLibs.BuildFilter(engineInfo, fldName, this.EmailOrPhone, varName, searchOption);
                }
            }

            internal bool Load(game.myGame.UserSubmitDataTable tbl, ReliableSqlConnection conn = null)
            {
                Build();
                string sqlCml = String.Format("SELECT* FROM {0} {1}",
                    DbCache.myGameDS.UserSubmit.TableName, EngineLibs.GetTableAlias(TblNames.UserSubmit));

                if (String.IsNullOrWhiteSpace(this.EngineInfo.FilterCmd) == false)
                    sqlCml += " WHERE " + this.EngineInfo.FilterCmd;

                this.EngineInfo.SetSqlCommandText(sqlCml);
                return EngineLibs.LoadFromSQL(tbl, this.EngineInfo.SqlCommand, conn);

            }

            internal short? GetRangeOfSubmit(BO.UserSubmit info, ReliableSqlConnection conn = null)
            {

                string varName = DbCache.myGameDS.UserSubmit.EmailOrPhoneColumn.ColumnName;
                string fldName = DbCache.myGameDS.UserSubmit.EmailOrPhoneColumn.ColumnName;
                DataAccess.SearchOptions searchOption = this.EngineInfo.GetSearchOption(TblNames.UserSubmit, varName, DataAccess.SearchOptions.Exact);
                EngineLibs.BuildFilter(this.EngineInfo, fldName, info.EmailOrPhone, varName, searchOption);

                string sqlCmd = String.Format("SELECT TOP 1 RowNo FROM (" +
                                                 Consts.constCRLF + " SELECT ROW_NUMBER() OVER( " +
                                                 Consts.constCRLF + " ORDER BY {1}.{2} ASC) AS RowNo, {1}.* FROM {0} {1}) a" +
                                                 Consts.constCRLF + " WHERE {3}",
                                                 EngineLibs.GetTableName(TblNames.UserSubmit),
                                                 "u",
                                                 DbCache.myGameDS.UserSubmit.PlayTimeColumn.ColumnName,
                                                 this.EngineInfo.FilterCmd);

                this.EngineInfo.SetSqlCommandText(sqlCmd);
                return Convert.ToInt16(DataAccess.EngineLibs.ExecuteScalarSQL(this.EngineInfo.SqlCommand));
            }
        }
    }
}



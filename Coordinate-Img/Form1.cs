﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace Coordinate_Img
{
    public partial class Form1 : Form
    {
        private const int MaxWidth = 800;
        private const int MaxHeight = 600;

        private class PicCursor : PictureBox
        {
            public PicCursor(Point location, string name)
            {
                Image img = Image.FromFile("star.png");
                this.Image = img;
                this.BackColor = Color.Transparent;
                this.Size = img.Size;
                this.Location = location;
                this.Name = name;
                //this.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }


        public Form1()
        {
            InitializeComponent();
            pictureBox1.Size = new Size(MaxWidth, MaxHeight);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileOnpen = new OpenFileDialog();
                fileOnpen.ShowDialog();

                string imgStr = fileOnpen.FileName;
                Image img = Image.FromFile(imgStr);
                pictureBox1.Image = img;
                pictureBox1.Size = new Size(img.Width, img.Height);
            }
            catch { }
        }

        private Point ConvertCursorPoint(Point point)
        {
            float percentWidth = pictureBox1.Image.Size.Width / MaxWidth;
            float percentHeight = pictureBox1.Image.Size.Height / MaxHeight;
            return new Point((int)Math.Ceiling(point.X * percentWidth), (int)Math.Ceiling(point.Y * percentHeight));
        }

        private int count = 0;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                MouseEventArgs me = (MouseEventArgs)e;
                string _count = (count++).ToString();

                PicCursor picCursor = new PicCursor(me.Location, "pic" + _count);
                pictureBox1.Controls.Add(picCursor);

                Point point = ConvertCursorPoint(me.Location);
                Size size = picCursor.Image.Size;

                dataGridView1.Rows.Add(_count,
                                        point.X.ToString(), point.Y.ToString(),
                                       size.Width.ToString(), size.Height.ToString());
            }
            catch
            {
                MessageBox.Show("Error!!");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                using (XmlWriter writer = XmlWriter.Create(textBox1.Text))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("spotList");

                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {

                        writer.WriteStartElement("spot");

                        writer.WriteElementString("x", (string)dataGridView1[1, i].Value);
                        writer.WriteElementString("y", (string)dataGridView1[2, i].Value);
                        writer.WriteElementString("width", (string)dataGridView1[3, i].Value);
                        writer.WriteElementString("height", (string)dataGridView1[4, i].Value);

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
                MessageBox.Show("Save successfully!");
            }
            catch
            {
                MessageBox.Show("Save Failed!");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            pictureBox1.Controls.Clear();
            dataGridView1.Rows.Clear();
            count = 0;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            try
            {
                DataCache.Libs.Reset(CommonTypes.CacheType.MemCache);
                MessageBox.Show("Reset cache successfully!");
            }
            catch
            {
                MessageBox.Show("Reset cache Failed!");
            }
            
        }
    }
}
